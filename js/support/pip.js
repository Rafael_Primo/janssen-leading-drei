var container = "liveContainer";
var pipBlock = document.getElementById("pip-blocker");
var divDraggable = "#liveContainer";
var position = [];
var disabledManually = false;
var isDragging = false;
var isDebug = true; //Debug for console logs.

//This will define which place the draggable was at.
var posString = ""

$("#dragHandler").on("mouseup",checkPlay);
$("#video-place-reference").on("videoReady", SetReferencePosition);

function SetReferencePosition()
{
  if (isDebug) //console.log("Applying reference position...");
  /*Get all coordinate information of the original video inside the site, copy it and stay on it's
   original position until PIP is disabled.
  This is important to keep everything in place once the video gets out of it's original spot.*/
  $("#video-place-reference").width($("#liveContainer").width());
  $("#video-place-reference").height($("#liveContainer").height());
  $("#video-place-reference").css(
    {
     'position' : 'absolute',
     'left' : $("#liveContainer").position().left + 'px',
    //  'top' : $("#liveContainer").position().top + 'px'
    })
}

function EnablePIP() {
  $("#close-pip").show();
  if (disabledManually) return;
  $("#" + container).removeClass("topLevel");
  $("#" + container).addClass("pip");
  $("#livevideo").addClass("pip");
  $(divDraggable).draggable({
    containment: "#pip-blocker",
    scroll: false,
    start: function(event,ui)
    {
      isDragging = true;
    },
    stop: function (event, ui) {
      ReadPosition();
      isDragging = false;
    },
    drag: function(event, ui)
    {
      FollowDragHandler();
    }
  });
}
function ClosePIP() {
  disabledManually = true;
  DisablePIP();
}

function ReadPosition() {
  position[0] = $(divDraggable).css("left");
  position[1] = $(divDraggable).css("top");

  //remove "px" from the values.
  var xInt = parseFloat(position[0]);
  var yInt = parseFloat(position[1]);

  //Temporary horizontal to concatenate the position words.
  var tempH = "";
  if (xInt >= window.innerWidth / 2) tempH = "R";
  else tempH = "L";

  //Temporary vertical to concatenate the position words.
  var tempV = "";
  if (yInt >= window.innerHeight / 2) tempV =  "D";
  else tempV = "U"

  posString = tempH + tempV;

  //console.log(position[0] + " " + position[1] + " screen: " + window.innerWidth + " " + window.innerHeight);

  if (posString == "RD") posString = "RU";

  SetPosition(posString);
}

function FollowDragHandler()
{
  position[0] = $(divDraggable).css("left");
  position[1] = $(divDraggable).css("top");

  $("#livevideo").attr('left', position[0]);
  $("#livevideo").attr('top', position [1]);
}

function SetPosition(position) {

  //Get window size
  let wx = window.innerWidth;
  let wy = window.innerHeight;

  //Get container size
  let vx = $("#liveContainer").width();
  let vy = $("#liveContainer").height();

  //console.log(position);

  switch (position) {
    case "LU":
      $("#" + container).animate({ left: "20px", top: "20px" }, 500);
      break;
    case "LD":
      $("#" + container).animate(
        { left: "20px", top: wy - vy - 20 + 'px' }, //Window height - Panel height - margin (20px)
        500
      );
      break;
    case "RU":
      $("#" + container).animate(
        { left: wx - vx - 40 + 'px', top: "20px" },
        500
      );
      break;
  }
}

function checkPlay()
{
  //console.log(isDragging);
  if (!isDragging)
  {
    if(m_VimeoLive == null){
      if (player.getPlayerState() != 1 ) 
      {
          player.playVideo();
      }
      else if(player.getPlayerState() == 1)
      {
          player.pauseVideo();
      }
    }
    else{
      m_VimeoLive.getPaused().then(function(isPaused){
  
          if (isPaused) 
          {
            m_VimeoLive.play();
          }
          else 
          {
            m_VimeoLive.pause();
          }
        
      })
    }
  }
}

function DisablePIP() {
  $("#close-pip").hide();
  $("#" + container).removeClass("pip");
  $("#" + container).addClass("topLevel");
  $("#livevideo").removeClass("pip");
}

function CheckMainVideoVisibility()
{
  videoVisible = $("#video-place-reference").visible(true);
  if (!videoVisible) {
    EnablePIP();
  } else {
    DisablePIP();
    disabledManually = false;
  }
}

$(document.body).on('touchmove', CheckMainVideoVisibility());
$(document).scroll(function (ev) {
  CheckMainVideoVisibility();
});
