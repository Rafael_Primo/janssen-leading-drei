const apiEndpoint = "https://29556qsz4c.execute-api.sa-east-1.amazonaws.com/dev/";

var HttpClientGet = function () {
    this.get = function (aUrl, aCallback) {
        var anHttpRequest = new XMLHttpRequest();
        anHttpRequest.onreadystatechange = function () {
            // alert("Status: " + anHttpRequest.status);
            if (anHttpRequest.readyState == 4 && anHttpRequest.status == 200)
                aCallback(anHttpRequest.responseText);
        }

        anHttpRequest.open("GET", aUrl, true);
        anHttpRequest.send(null);
    }
}
var HttpClientPost = function () {
    this.post = function (aUrl, aCallback) {
        var anHttpRequest = new XMLHttpRequest();
        anHttpRequest.onreadystatechange = function () {
            // alert("Status: " + anHttpRequest.status);
            if (anHttpRequest.readyState == 4 && anHttpRequest.status == 200)
                aCallback(anHttpRequest.responseText);
        }

        anHttpRequest.open("POST", aUrl, true);
        anHttpRequest.send(null);
    }
}

function CheckDate(_val) {
    // if (!stopCounter) {
    // }
    let url = apiEndpoint + "servertime" + "?v=" + _val;

    var client = new HttpClientGet();

    client.get(url, function (response) {
        let temp = JSON.parse(response);
        console.log(temp);
        if(_val == "crono" || _val == "cronoT"){
            m_now[0] = temp[0];
            m_now[1] = temp[1];
            m_now[2] = temp[2];
            m_now[3] = temp[3];
            m_now[4] = temp[4];
            // m_now[5] = temp[5];
        }
        else{
            now = temp;
            Counter(temp);
        }
    });
}



// function PostData(_Email, _Name, _Surname, _Crm, _Uf) {
//     let url = apiEndpoint + "insertuser" + "?e=" + _Email.toLowerCase() + "&n=" + _Name + " " + _Surname + "&c=" + _Crm + "&u=" + _Uf;
//     var client = new HttpClientPost();
//     client.post(url, function (response) {
//         let temp = JSON.parse(response);
//         // console.log(temp)
//         if(temp == 0)
//         {
//             $('#feedback').text("usuário já registrado / e-mail já em uso");
//             $('#feedback').show();
//             $("#email").val("");
//             $("#nome").val("");
//             $("#sobrenome").val("");
//             $("#crm").val("");
//             $("#uf").val("");
//             $('#btnCadastro').prop('disabled', true);
//             $('#loaderStart').hide();
//         }
//         else
//         {
//             $("#email").val("");
//             $("#nome").val("");
//             $("#sobrenome").val("");
//             $("#crm").val("");
//             $("#uf").val("");
//             $('#btnCadastro').prop('disabled', true);
//             TrackUser(_Email, "Realizou o Cadastro");
//             CaptureAnalytics("/Cadastro/Concluido", "Realizou o Cadastro");
//             $('#loaderStart').hide();
//             $("#emailFeedback").text("Cadastrado com sucesso. Nos vemos no dia 28/08");
//             $("#emailFeedback").css("background-color", "#2f792f");
//             $("#emailFeedback").show();
//             $('#cadastro').hide();
//             $('#login').show();
//         }
//     });
// }
function PostData(_Email, _Name, _Surname, _Crm, _Uf) {
    let url = apiEndpoint + "createuser" + "?e=" + _Email.toLowerCase() + "&n=" + _Name + " " + _Surname + "&c=" + _Crm + "&u=" + _Uf;
    var client = new HttpClientPost();
    client.post(url, function (response) {
        let temp = JSON.parse(response);
        // console.log(temp)
        if(temp == 0)
        {
            $('#feedback').text("usuário já registrado / e-mail já em uso");
            $('#feedback').show();
            $("#email").val("");
            $("#nome").val("");
            $("#sobrenome").val("");
            $("#crm").val("");
            $("#uf").val("");
            $('#btnCadastro').prop('disabled', true);
            $('#loaderStart').hide();
        }
        else
        {
            $("#email").val("");
            $("#nome").val("");
            $("#sobrenome").val("");
            $("#crm").val("");
            $("#uf").val("");
            $('#btnCadastro').prop('disabled', true);
            TrackUser(_Email, "Realizou o Cadastro");
            CaptureAnalytics("/Cadastro/Concluido", "Realizou o Cadastro");
            $('#loaderStart').hide();
            $("#emailFeedback").text("Cadastrado com sucesso. Nos vemos no dia 28/08");
            $("#emailFeedback").css("background-color", "#2f792f");
            $("#emailFeedback").show();
            $('#cadastro').hide();
            $('#login').show();
        }
    });
}

function CheckUserAuth(_login) {
    let url = apiEndpoint + "userauth" + "?l=" + _login.toLowerCase();

                $("#loaderStart").show();
    var client = new HttpClientGet();
    client.get(url, function (response) {
        let temp = JSON.parse(response);
        // console.log(temp);
        
        if (temp == 1) {
            //email certo
                location.href = 'home.html?l=' + _login.toLowerCase() + "&on=1";
                // $("#emailFeedback").text("Você já está cadastrado. Nos vemos no dia 28/08");
                // $("#emailFeedback").show();
                // $("#loaderStart").hide();
        } else if (temp == 0) {
            //email invalido
            $("#login").hide();
            $("#cadastro").show();
            $("#email").val(_login);
            CaptureAnalytics("/Cadastro/Cadastro", "Abriu o Cadastro");
            $("#loaderStart").hide();
        }
        else if(temp == -1) {
            //email n existe no banco
            $("#login").hide();
            $("#cadastro").show();
            $("#email").val(_login);
            CaptureAnalytics("/Cadastro/Cadastro", "Abriu o Cadastro");
            $("#loaderStart").hide();
            // $("#emailFeedback").text("Seu e-mail não está cadastrado. Por favor, entre em contato com o seu representante Janssen.");
            // $("#emailFeedback").css("background-color", "#E6332A");
            // $("#emailFeedback").show();
            // $("#loaderStart").hide();
        }   
        else {
            $("#emailFeedback").text("Houve algum erro no login. Por favor, tente novamente");
            $("#emailFeedback").css("background-color", "#E6332A");
            $("#emailFeedback").show();
            $("#loaderStart").hide();
        }       
    });
}
var userGroup;
function CheckUserRoom(_login) {
    let url = apiEndpoint + "checkuserroom" + "?l=" + _login.toLowerCase();
    var client = new HttpClientGet();
    client.get(url, function (response) {
        let temp = JSON.parse(response);
        // console.log(temp);
        
        if (temp == -1) {
            console.log("erro")
        } else if (temp[0] == 1) {
            userGroup = temp[1];
        }
        else {
            console.log("algum erro desconhecido: " + temp)
        }       
    });
}



function CheckUserOnline(_login, _online) {
    let url = apiEndpoint + "userauth/onauth" + "?l=" + _login.toLowerCase() + "&on=" + _online;

    var client = new HttpClientGet();
    client.get(url, function (response) {
        let temp = JSON.parse(response);

            console.log(temp);
        if (temp == -1) {
            //console.log("senha incorreta");
            location.href = 'index.html';
        } else if (temp == 0) {
            //console.log("email incorreto");
            location.href = 'index.html';
        } else if (temp[0] == 1) {
            //console.log("dados conferem");
            var newURL = removeParam("l", window.location.href);
            newURL = removeParam("on", newURL);
            newURL = newURL.replace("?", "");
            window.history.pushState('', document.title, newURL);
            username = temp[1][0].name;

            TrackUser(_login, "Entrou no 360");
            CheckUserRoom(_login);
            $("#loaderStart").hide();
        } else {
            // location.href = 'index.html';
        }
    });
}

function GetLink(_link) {
    let url = apiEndpoint + "livelink" + "?l=" + _link;
    var client = new HttpClientGet();
    client.get(url, function (response) {
        let temp = JSON.parse(response);
        // console.log(temp);
        if(_link == "pesquisa"){
            $("#pesquisaLink").attr("onclick", "ButtonClick('" + _link + "', '" + temp + "')");
        }
        else if(_link == "plenaria"){
            langVideo = "pt";
            vimeoLang = "pt";
            videoLinkPT = temp[0];
            videoLinkEN = temp[1];
            OpenModal(videoLinkPT);
            // $("#closePlenaria").attr("onclick", "ButtonClick('" + _link + "', '" + videoLinkPT + "')");
            // $("#textPlenaria").attr("onclick", "ButtonClick('" + _link + "', '" + videoLinkPT + "')");
        }
        else if(_link == "plenariasala1"){
            langVideo = "pt";
            vimeoLang = "pt";
            videoLinkPT = temp[0];
            videoLinkEN = temp[1];
            OpenModal(videoLinkPT);
        }
        else if(_link == "plenariasala2"){
            langVideo = "pt";
            vimeoLang = "pt";
            videoLinkPT = temp[0];
            videoLinkEN = temp[1];
            OpenModal(videoLinkPT);
        }
        else if(_link == "plenariasala3"){
            langVideo = "pt";
            vimeoLang = "pt";
            videoLinkPT = temp[0];
            videoLinkEN = temp[1];
            OpenModal(videoLinkPT);
        }
        else if(_link == "sala1"){
            langVideo = "pt";
            vimeoLang = "pt";
            videoLinkPT = temp[0];
            videoLinkEN = temp[1];
            OpenModal(videoLinkPT);
            $('#plenariaModal2').modal('hide');
        }
        else if(_link == "sala2"){
            langVideo = "pt";
            vimeoLang = "pt";
            videoLinkPT = temp[0];
            videoLinkEN = temp[1];
            OpenModal(videoLinkPT);
            $('#plenariaModal2').modal('hide');
        }
        else if(_link == "sala3"){
            langVideo = "pt";
            vimeoLang = "pt";
            videoLinkPT = temp[0];
            videoLinkEN = temp[1];
            OpenModal(videoLinkPT);
            $('#plenariaModal2').modal('hide');
            // urlChat = "https://minnit.chat/janssenleading?embed&&language=pt&channel=sala3&nickname=" + username;
            // document.getElementById("chatIframe").src = urlChat;
        
            // $('#chatIframe').prepend('<iframe src="' + urlChat + '" allowTransparency="true">');
        }
        else if(_link == "links"){
            $("#carLink0").attr("onclick", "ButtonClick('Endoscopia', '" + temp[0] + "')");
            $("#carLink1").attr("onclick", "ButtonClick('Medical', '" + temp[1] + "')");
            $("#carLink2").attr("onclick", "ButtonClick('IDB', '" + temp[2] + "')");
            $("#carLink3").attr("onclick", "ButtonClick('DDW', '" + temp[3] + "')");
            $("#carLink4").attr("onclick", "ButtonClick('Global', '" + temp[4] + "')");
            $("#carLink5").attr("onclick", "ButtonClick('Pro', '" + temp[5] + "')");
            $("#carLink6").attr("onclick", "ButtonClick('Controversias', '" + temp[6] + "')");
            $("#carLink7").attr("onclick", "ButtonClick('Vacina', '" + temp[7] + "')");
        }
    });
}



function TrackUser(_login, _action) {
    let url = apiEndpoint + "usertracking" + "?l=" + _login.toLowerCase() + "&a=" + _action;

    var client = new HttpClientGet();
    client.get(url, function (response) {
        let temp = JSON.parse(response);
        // console.log(temp);

        if (temp == 1) {
            // console.log("inseriu no banco");
        } else {
            // console.log("houve algum problema");
        }
    });
}

function AddListenerEnter(field, button) {
    input = document.getElementById(field);
    input.addEventListener("keyup", function (event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            document.getElementById(button).click();
        }
    });
}

function ValidateEmail(email) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,99})+$/.test(email)) {
        return (true);
    }
    return (false);
}

function iOS() {
    return [
        'iPad Simulator',
        'iPhone Simulator',
        'iPod Simulator',
        'iPad',
        'iPhone',
        'iPod'
    ].includes(navigator.platform) || (navigator.userAgent.includes("Mac") && "ontouchend" in document)
}

