const m_GoogleAnalyticsID = "UA-164821163-36"; //Final
const m_DebugAnalytics = false;

function gaInit(_UserID)
{
    $.getScript('//www.google-analytics.com/analytics.js');
    window.ga = window.ga || function () { (ga.q = ga.q || []).push(arguments) }; ga.l = + new Date;
    ga('create', m_GoogleAnalyticsID, 'auto', { 'userId': _UserID });
    ////console.log("Initalized");
    return ga;
};

function gaTrack(path, title, _UserID)
{
    var track =  { page: path, title: title};

    if(m_DebugAnalytics == true)
    {
        console.log("Debug Analytics Tracked");
    }
    else
    {
        ga = window.ga || gaInit(_UserID);
        ga('set', track);
        ga('send', 'pageview');
        //console.log("Tracked");
    }
};


function CaptureAnalytics(_Path, _PageName)
{
    gaTrack(_Path, _PageName, "Visitas");
}