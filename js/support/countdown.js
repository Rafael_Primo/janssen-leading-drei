var days;
var hours;
var minutes;
var seconds;

var x;
var now;

var m_counter;
var m_horas;
var m_min;
var m_seg;

var stopCounter = false;
var hideLoader = false;
var canLogin = false;

var lastValue = null;
var lastValueH = null;
var lastValueM = null;
var lastValueS = null;

function Counter(_timestamp) {  
  // m_counter = document.getElementById("countdownTimer");
  m_horas = document.getElementById("countdownHora");
  m_min = document.getElementById("countdownMin");
  m_seg = document.getElementById("countdownSeg");
  hours = Math.floor((_timestamp / (1000 * 60 * 60)));
  minutes = Math.floor((_timestamp % (1000 * 60 * 60)) / (1000 * 60));
  seconds = Math.floor((_timestamp % (1000 * 60)) / 1000);

  if (hours < 10) {
    hours = "0" + hours;
  }
  if (minutes < 10) {
    minutes = "0" + minutes;
  }
  if (seconds < 10) {
    seconds = "0" + seconds;
  }

  // feito pra funcionar, reative
  // m_horas.innerText = hours;
  // m_min.innerText = minutes;
  // m_seg.innerText = seconds;

  // if(lastValue == null){
  //   m_counter.innerText = "00:00:00";
  // } else{
  //   m_counter.innerText = lastValue;
  // }
  if(lastValueH == null){
    // m_horas.innerText = "00";
  } else{
    // m_horas.innerText = lastValueH;
  }
  if(lastValueM == null){
    // m_min.innerText = "00";
  } else{
    // m_min.innerText = lastValueM;
  }
  if(lastValueS == null){
    // m_seg.innerText = "00";
  } else{
    // m_seg.innerText = lastValueS;
  }

  now = _timestamp;

  clearInterval(x);

  x = setInterval(function () {

    now = now - 1000;

    var distance = now;

    Cronometro(distance);
  }, 1000);
}

function Cronometro(distance) {
  hours = Math.floor((distance / (1000 * 60 * 60)));
  minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  seconds = Math.floor((distance % (1000 * 60)) / 1000);  

  if (hours < 10) {
    hours = "0" + hours;
  }
  if (minutes < 10) {
    minutes = "0" + minutes;
  }
  if (seconds < 10) {
    seconds = "0" + seconds;
  }

  // m_counter.innerText = hours + ":" + minutes + ":" + seconds;
  // m_horas.innerText = hours;
  // m_min.innerText = minutes;
  // m_seg.innerText = seconds;

  // lastValueH = m_horas.innerText;
  // lastValueM = m_min.innerText;
  // lastValueS = m_seg.innerText;

  if(!hideLoader){
    hideLoader = true;

    $("#loaderStart").hide();
  }

  if (distance < 0) {
    clearInterval(x);
    // m_counter.innerText = "00:00:00";
    // m_horas.innerText = "00";
    // m_min.innerText = "00";
    // m_seg.innerText = "00";
    canLogin = true;
    stopCounter = true;
    StartAll();
  } else{
    // m_counter.innerText = hours + ":" + minutes + ":" + seconds;
    // m_horas.innerText = hours;
    // m_min.innerText = minutes;
    // m_seg.innerText = seconds;
    StartAll();

    // lastValue = m_counter.innerText;
    // lastValueH = m_horas.innerText;
    // lastValueM = m_min.innerText;
    // lastValueS = m_seg.innerText;
  }
}