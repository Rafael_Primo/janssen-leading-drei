////////////////////////////////////////////GLOBAL VARIABLES////////////////////////////////////////////
var m_Clock;
var m_Viewer;
var m_controler;
var m_PanolensScene;
var m_PanolensSceneTextureDirectory;
var m_Zoom;
var m_PanoloensLoaded = false;
var m_SpotInZoom;
var m_ClockUpdate;
var m_IsAfternoon = false;
var m_VimeoLive = null;

var m_PlenariaOpen = false;
var m_PlenariaAfterOpen = false;
var m_PlenariaFinished = false;
var m_RoomShowed = false;
var m_RoomPopShowed = false;
var m_SatisfactionShow = false;

var m_objects = [];

var m_Mute = false; //Disable all audio from being played.
var m_MutesOnInteraction = false; //Does the audio gets muted if the user presses a infospot?

var m_AudioSource = new Audio(); //Audio player. Set it's source to define the audio to be played.


//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
var debug_ignoreLinkSpots = false; //!!!!!REMEMBER TO TURN THIS TO FALSE IF NOT DEBUGGING LINK SPOTS!!!!!!!!!!!!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

var m_selectedLinkSpot;

var m_UpdateTimerStart;
var m_now = [];

var m_SpotIndex = [];

var m_ActualScene;

var isDebug = true;
var loadedImages = [];
var lastImage;

var m_SpriteObjects =
{
    "value":
    [
        {
            //text
            "texture": './imgs/alphablock.png',
            "body": null,
        },
        {
            //text
            "texture": './imgs/alphablock.png',
            "body": null,
        },
        {
            //text
            "texture": './imgs/alphablock.png',
            "body": null,
        },
        {
            //text
            "texture": './imgs/alphablock.png',
            "body": null,
        },
        {
            //text
            "texture": './imgs/alphablock.png',
            "body": null,
        },
        {
            //text
            "texture": './imgs/alphablock.png',
            "body": null,
        },
        {
            //text
            "texture": './imgs/alphablock.png',
            "body": null,
        }
    ]
};

var m_ClockTimerTextObject = 
{
    "value":
    [
        // {
        //     "text": "88:88:88",
        //     "canvas": null,
        //     "texture": null,
        //     "body": null,
        //     "textWidth": 300,
        //     "textHeight": 100,
        //     "textSize": 50,
        //     "font": "DS-DIGI-REGULAR",
        //     "textColor": "#ffffff", 
        //     "x": -1050, //+500
        //     "y": 900, //-20
        //     "z": 4000, //-100
        //     "rx": (5 * Math.PI / 180),
        //     "ry": (-125 * Math.PI / 180),
        //     "rz": (0 * Math.PI / 180),
        //     "frx": 0 * Math.PI / 180,
        //     "fry": -90 * Math.PI / 180,
        // },
        // {
        //     "text": "88:88:88",
        //     "canvas": null,
        //     "texture": null,
        //     "body": null,
        //     "textWidth": 300,
        //     "textHeight": 100,
        //     "textSize": 50,
        //     "font": "DS-DIGI-REGULAR",
        //     "textColor": "#ffffff", 
        //     "x": -1050, //+500
        //     "y": 950, //-20
        //     "z": 4000, //-100
        //     "rx": (5 * Math.PI / 180),
        //     "ry": (-125 * Math.PI / 180),
        //     "rz": (0 * Math.PI / 180),
        //     "frx": 0 * Math.PI / 180,
        //     "fry": -90 * Math.PI / 180,
        // },
        {
            "text": "88:88:88",
            "canvas": null,
            "texture": null,
            "body": null,
            "textWidth": 300,
            "textHeight": 100,
            "textSize": 50,
            "font": "DS-DIGI-REGULAR",
            "textColor": "#ffffff", 
            "x": -2000, //+500
            "y": 500, //-20
            "z": 3500, //-100
            "rx": (-1 * Math.PI / 180),
            "ry": (-150 * Math.PI / 180),
            "rz": (0 * Math.PI / 180),
            "frx": 0 * Math.PI / 180,
            "fry": -90 * Math.PI / 180,
        },
    ]
};

var m_ClockFeedbackTextObject = 
{
    "value":
    [
        {
            //Manhã
            "text": "EM BREVE",
            "canvas": null,
            "texture": null,
            "body": null,
            "textWidth": 300,
            "textHeight": 100,
            "textSize": 35,
            "font": "Gotham HTF",
            "fontWeight": "normal",
            "textColor": "#233792", 
            "x": 2388.6,    //+500
            "y": 605.0,    //-20
            "z": 3375.7, //-100
            // "x": 2392.1,   //+500
            // "y": 638.6,   //-20
            // "z": 3378.1, //-100
            "rx": 0,
            "ry": -80 * Math.PI / 180,
            "rz": 0,
            "frx": 0 * Math.PI / 180,
            "fry": -90 * Math.PI / 180,
        },
        {
            //Manhã
            "text": "EM BREVE",
            "canvas": null,
            "texture": null,
            "body": null,
            "textWidth": 300,
            "textHeight": 100,
            "textSize": 35,
            "font": "Gotham HTF",
            "fontWeight": "normal",
            "textColor": "#233792", 
            "x": 3554.9,    //+500
            "y": 589.0,   //-20
            "z": 2767.7, //-100
            // "x": 3550.2,   //+500
            // "y": 615.0,   //-20
            // "z": 2780.4, //-100
            "rx": 0,
            "ry": -50 * Math.PI / 180,
            "rz": 0,
            "frx": 0 * Math.PI / 180,
            "fry": -90 * Math.PI / 180,
        }
    ]
};

function iOS() {
    return [
        'iPad Simulator',
        'iPhone Simulator',
        'iPod Simulator',
        'iPad',
        'iPhone',
        'iPod'
    ].includes(navigator.platform) || (navigator.userAgent.includes("Mac") && "ontouchend" in document)
}
function ClearVideo()
{
    /* $("#chat").hide(); */
    $("#idioma").hide();
    $("#wppBtn").show();
    m_VimeoLive = ""
    $('#iframeModal').off("load");
    $("#iframeModal").attr("src", "");
    ButtonClick("ChatCloseEspecial");
    ButtonClick("idiomaCloseEspecial");
}
function ClearVideoPlenaria()
{
    /* $("#chat").hide(); */
    $("#idioma").hide();
    $("#wppBtn").show();
    m_VimeoLive = ""
    $('#iframeModalPlenaria').off("load");
    $("#iframeModalPlenaria").attr("src", "");
    ButtonClick("ChatCloseEspecial");
    ButtonClick("idiomaCloseEspecial");
}


function MoveImage(sprite,px, py, pz){
    sprite.body.position.set(px,py,pz);
}

function RotateObject(object, degreesX, degreesY, degreesZ)
{
    object.rotation.set((degreesX * Math.PI / 180), (degreesY * Math.PI / 180), (degreesZ * Math.PI / 180));
}
function MoveObject(object, x, y, z)
{
    object.position.set (x,y,z);
}


function RotateImage(sprite,x, y, z, fx){
    sprite.body.rotation.set((x * Math.PI /180), (y * Math.PI /180), (z * Math.PI /180));
}


var texture;
var material;

var m_SpriteObject;


////////////////////////////////////////EVENTS////////////////////

const eventModalClose = new Event('modalClose');

addEventListener("modalClose",
function()
{
  if (m_MutesOnInteraction) ResumeAudio();
})


////////////////////////////////////////////METHODS///////////////////////////////////////////
function PlayAudio(_name, _loop) //Loads and plays an audio using m_AudioSource.
{
    //if (isDebug) console.log("PLAYING AUDIO");
    let isPlaying = m_AudioSource.paused;
    if (isPlaying) StopAudio();
    m_AudioSource.volume = 0.2;
    m_AudioSource.src = "./audio/" + _name;
    m_AudioSource.loop = _loop;
    m_AudioSource.play();
}
function ResumeAudio()
{
   if (m_AudioSource.src != "" || m_AudioSource.src != undefined) m_AudioSource.play();
}
function PauseAudio()
{
    //if (isDebug) console.log("PAUSE AUDIO");
    let isPaused = m_AudioSource.paused;
    if (!isPaused)
    {
        m_AudioSource.pause();
    }
}
function StopAudio()
{

    let isPlaying = m_AudioSource.paused;
    if (isPlaying)
    {
        m_AudioSource.pause();
        m_AudioSource.currentTime = 0;
        m_AudioSource.src = "";
    }
}
function ToggleAudio() //Toggles sound mute on or off.
{
    if (!m_Mute) $("#audioToggle").addClass("muted");
    else $("#audioToggle").removeClass("muted");
    m_Mute = !m_Mute;
    m_AudioSource.muted = m_Mute;

    if (!m_Mute) ResumeAudio();
}
function ToggleHUD()
{
    $("#audioToggle").toggle();
    $("#buttons-shared").toggle();
    $("#logo360").toggle();
    if (m_ActualScene != 0) $("#buttonBack").toggle();
}
function CheckActiveSceneAndPlayAudio() //Check the current scene the user is at and play a specific audio for it
{
    return;
    // console.log("checking scene...");
    var _bubble = m_ActualScene + 1;

    switch (_bubble)
    {
        case 1:
          if (!iOS()) {PlayAudio("music_placeholder.mp3", true);}
        break;
        case 2:
            PlayAudio("music_placeholder.mp3", true);
        break;
    }
}

function LoaderPage(_Enable)
{
    if(_Enable)
    {
        $("#loaderStart").show()
        let stateCheck = setInterval(() =>
        {
            if (document.readyState === 'complete')
            {
                //console.log("complete");

                var delay = setInterval(function()
                {
                    $('#loaderStart').hide();
                    m_PanoloensLoaded = true;
                    clearInterval(delay);
                    setTimeout(() => {
                        $('#loaderStart').hide();
                    }, 1000);
                }, 5000);

                clearInterval(stateCheck);
            }
        }, 100);
    }
    else
    {
        //var loader = document.getElementById("loader");
        //loader.style.display = "none";
        m_PanoloensLoaded = true;
    }
}



THREE.Object3D.prototype.getObjectsByTag = function( tag, result ) {

    // check the current object

    if ( this.userData.tag === tag ) result.push( this );

    // check children

      for ( var i = 0, l = this.children.length; i < l; i ++ ) {

          var child = this.children[ i ];

      child.getObjectsByTag( tag, result );

      }

    return result;

};


//handle mouse raycast
function onDocumentMouseMove(event) {

    var mouse = new THREE.Vector2();
    mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
    mouse.y = - (event.clientY / window.innerHeight) * 2 + 1;

    m_raycaster = new THREE.Raycaster();

    m_raycaster.setFromCamera(mouse, m_Viewer.camera);
    var intersects = m_raycaster.intersectObjects(m_PanolensScene[m_ActualScene].getObjectsByTag("feedback", []));
    if (intersects.length > 0) {
        $('.panolens-container').css('cursor', 'pointer');
    } else {
        $('.panolens-container').css('cursor', 'default');
    }

}
function onReturnToPage()
{
    if(m_CanRunAnalytics)
    {
        m_CanRunAnalytics = false;
        AnalyticsCallOnFade();
    }
}

function AddSpriteSheet(_PanolensScene, _SpriteSheetObject, _Plane, _Tag, _Interaction, _CaseName)//New Method using update panalones to render
{
    let loader = new THREE.TextureLoader();
    _SpriteSheetObject.object = loader.load(_SpriteSheetObject.spritesheet);
    _SpriteSheetObject.object.minFilter = THREE.LinearFilter;

    _SpriteSheetObject.object.repeat.x = 1 / _SpriteSheetObject.cols;
    _SpriteSheetObject.object.repeat.y = 1 / _SpriteSheetObject.rows;

    let material = new THREE.MeshBasicMaterial( { map: _SpriteSheetObject.object, transparent: true, alphaTest: 0, opacity: 1} );
    _SpriteSheetObject.body = new THREE.Mesh( _Plane, material );
    _SpriteSheetObject.body.position.set(_SpriteSheetObject.x, _SpriteSheetObject.y, _SpriteSheetObject.z);
    _SpriteSheetObject.body.rotation.set(_SpriteSheetObject.rx, _SpriteSheetObject.ry, _SpriteSheetObject.rz);
    _SpriteSheetObject.body.userData.tag = _Tag;
    if(_Interaction)
    {
        _SpriteSheetObject.body.userData.tag = "feedback";
        switch (_CaseName) {
            case "portalExp1":
                // _SpriteSheetObject.body.addEventListener('click', SpriteChangeScene.bind(this, _PanolensScene,_SpriteSheetObject.x, _SpriteSheetObject.y, _SpriteSheetObject.z, m_PanolensScene[3]))
            break;

            default:
            break;
        }
    }
    material.needsUpdate = true;
    _PanolensScene.add( _SpriteSheetObject.body );
}

function SpriteChangeScene(_Scene, _X, _Y, _Z, _SceneToGO)
{
    let linkInvisible = new PANOLENS.Infospot(1);
    linkInvisible.position.set(_X, _Y, _Z);
    _Scene.add(linkInvisible);
    linkInvisible.focus( 1, TWEEN.Easing.Exponential.InOut );
    DelayZoomIn();
    if(_SceneToGO == m_PanolensScene[0]) {
        //CaptureAnalytics("/Bolha Experiência", "Experiências Globoplay");
    }
    setTimeout(() => {
        $('#loaderStart').show()
        $('#loaderStart').animate({opacity: 1}, 1000, function()
        {
            m_Viewer.setPanorama(_SceneToGO);
        });
    }, 1200);
}

function GoToBubble(_bubbleNumber)
{
    setTimeout(() => {
        $('#loaderStart').show()
        $('#loaderStart').animate({opacity: 1}, 1000, function()
        {
            m_Viewer.setPanorama(m_PanolensScene[_bubbleNumber]);
        });
    }, 1200);
}

function CreateLinkSpot(_PanoScene, _IconLink, _Tag, _IconSize, _PositionX, _PositionY, _PositionZ, _SceneToGo, _NeedEspecialFocus, _FocusX, _FocusY, _FocusZ)
{
    var image = _IconLink;
    const loader = new THREE.TextureLoader();
    loader.load(image,

        // onLoad callback
        function ( texture )
        {
            texture.minFilter = THREE.LinearFilter;

            let LinkSpot = new PANOLENS.Infospot(_IconSize, image);
            LinkSpot.userData.tag = _Tag;
            LinkSpot.userData.desiredScene = _SceneToGo.src;
            LinkSpot.position.set(_PositionX, _PositionY, _PositionZ);
            _PanoScene.add(LinkSpot);

            if(!_NeedEspecialFocus)
            {

                LinkSpot.addEventListener('click', function()
                {

                    if (debug_ignoreLinkSpots && isDebug)
                    {
                        console.error("Selected linkSpot: " + m_selectedLinkSpot);
                        console.error("Remember to turn debug_ignoreLinkSpots to false!");
                        m_selectedLinkSpot = LinkSpot;
                        return;
                    }
                    else
                    {
                      LinkSpot.focus( 1, TWEEN.Easing.Exponential.InOut );
                      DelayZoomIn();
                      setTimeout(() => {
                          $('#loaderStart').show()
                          if (!m_AudioSource.paused) StopAudio();
                          $('#loaderStart').animate({opacity: 1}, 1000, function()
                          {
                              m_Viewer.setPanorama(_SceneToGo);
                          });
                      }, 1200);
                    }
                });
            }
            else
            {
                LinkSpot.addEventListener('click', function()
                {
                    if (debug_ignoreLinkSpots && isDebug)
                    {
                        console.error("Selected linkSpot: " + m_selectedLinkSpot);
                        console.error("Remember to turn debug_ignoreLinkSpots to false!");
                        m_selectedLinkSpot = LinkSpot;
                        return;
                    }
                    else
                    {
                        let linkInvisible = new PANOLENS.Infospot(1);
                        linkInvisible.position.set(_FocusX, _FocusY, _FocusZ);
                        _PanoScene.add(linkInvisible);
                        linkInvisible.focus( 1, TWEEN.Easing.Exponential.InOut );
                        DelayZoomIn();
                        setTimeout(() => {
                            $('#loaderStart').show()
                            $('#loaderStart').animate({opacity: 1}, 1000, function()
                            {
                                m_Viewer.setPanorama(_SceneToGo);
                            });
                        }, 1200);
                    }
                });
            }
        },
        undefined,

        function ( err )
        {
            console.error( 'An error happened when try load image' );
        }
    );
}


function AnalyticsCallOnFade()
{
    //console.log("Call on fade: " + m_ActualScene);
    switch (m_ActualScene)
    {
        case 0:
            //console.log(m_ActualScene)
            // CaptureAnalytics("/Bolha 01 - Precursores", "Bolha 01 - Precursores");
        break;
        case 1:
            //console.log(m_ActualScene)
            // CaptureAnalytics("/Bolha 02 - O Cinema Novo", "Bolha 02 - O Cinema Novo");
        break;
        default:
        break;
    }
}

function CreateSpecificSpot(_PanoScene, _IconLink, _Tag, _IconSize, _PositionX, _PositionY, _PositionZ, _SpotIndex,  _Interaction, _CaseName, _HoverText, _NeedZoom, _PathAnalytics, _PathName)
{
    var image = _IconLink;
    const loader = new THREE.TextureLoader();
    loader.load(image,

        // onLoad callback
        function ( texture )
        {

            texture.minFilter = THREE.LinearFilter;
            m_SpotIndex[_SpotIndex] = new PANOLENS.Infospot(_IconSize, image);
            m_SpotIndex[_SpotIndex].name = _SpotIndex;
            m_SpotIndex[_SpotIndex].userData.tag = _Tag;
            m_SpotIndex[_SpotIndex].position.set(_PositionX, _PositionY, _PositionZ);
            if (_HoverText != null)
            {
                m_SpotIndex[_SpotIndex].addHoverText(_HoverText);
            }
            _PanoScene.add(m_SpotIndex[_SpotIndex]);

            m_SpotIndex[_SpotIndex].addEventListener('click', function()
            {
                m_SpotIndex[_SpotIndex].focus( 1, TWEEN.Easing.Exponential.InOut );
                if(_NeedZoom)
                {
                    DelayZoomIn();
                }
            });

            if(_Interaction)
            {
                m_SpotIndex[_SpotIndex].addEventListener('click', UnhideText.bind(this, _CaseName, _PathAnalytics, _PathName));
            }
        },
        undefined,

        function ( err )
        {
            console.error( 'An error happened when try load image' +_IconLink );
        }
    );
}

function RenderSpriteSheet(_SpriteSheetObject)
{
    if ( _SpriteSheetObject.object != null)
    {
        let t = m_Clock.getElapsedTime() * _SpriteSheetObject.fps;
        let imageIndex = Math.floor( t % _SpriteSheetObject.totalSheets );
        let col = imageIndex % _SpriteSheetObject.cols;
        let row = Math.floor( imageIndex / _SpriteSheetObject.cols );

        _SpriteSheetObject.object.offset.x =	col / _SpriteSheetObject.cols;
        _SpriteSheetObject.object.offset.y = 1 - ( ( 1 + row ) / _SpriteSheetObject.rows );

        _SpriteSheetObject.object.minFilter = THREE.LinearFilter;
    }
}


//Link Scenes Event
function LinkAndEvents()
{
    let setup =
    {
        "positions":
        [
            {
                //0
                "x": 5000.00,
                "y": 0,
                "z": 0,
                "a": 0,
            },
            {
                //1
                "x": 5000.00,
                "y": 0,
                "z": 0,
                "a": 0,
            }
        ]
    };


    for(let i = 0; i < m_PanolensScene.length; i++)
    {
        m_PanolensScene[i].addEventListener('enter-fade-start',function()
        {
            if(iOS()){
            }

            //console.log("enterfadestart");
            m_Viewer.tweenControlCenter(new THREE.Vector3(setup.positions[i].x, setup.positions[i].y, setup.positions[i].z), setup.positions[i].a);
            m_Viewer.OrbitControls.noZoom = false;
            ConfigFadeEvent();
            m_ActualScene = i;
            AnalyticsCallOnFade();
        });
        m_PanolensScene[i].addEventListener('leave-start', function(){
          // HideSprites(i);
        });
    }
    // m_PanolensScene[0].addEventListener('enter-start', ShowClocks.bind(this,true));
    m_PanolensScene[0].addEventListener('enter-fade-start', ShowClocks.bind(this,true));
    // m_PanolensScene[0].addEventListener('leave-start', ShowClocks.bind(this,false));
}

function ConfigFadeEvent(_Zoom)
{
    m_Zoom = m_controler.maxFov;
    m_Viewer.setCameraFov(m_Zoom);

    $('#loaderStart').animate({opacity: 0}, function(){
        $('#loaderStart').hide();
        m_SpotInZoom = false;
    });
}

//Create Plane object
function CreatePlane(X, Y, Z, H)
{
    let plane = new THREE.PlaneGeometry(X, Y, Z, H);
    return plane;
}

//Add Video in Plane texture
function AddVideo(_VideoObject, _Plane, _PanolensScene, _Loop, _Volume)
{
    _VideoObject.value[0].video = document.createElement( 'video' );
    _VideoObject.value[0].video.setAttribute("playsinline", "");
    _VideoObject.value[0].video.setAttribute("preload", "auto");
    _VideoObject.value[0].video.src = _VideoObject.value[0].videoIdleURL;
    _VideoObject.value[0].video.load();
    _VideoObject.value[0].video.volume = _Volume;
    _VideoObject.value[0].video.loop = _Loop;

    _VideoObject.value[0].texture = new THREE.VideoTexture( _VideoObject.value[0].video );
    _VideoObject.value[0].texture.minFilter = THREE.LinearFilter;
    _VideoObject.value[0].texture.magFilter = THREE.LinearFilter;
    _VideoObject.value[0].texture.format = THREE.RGBFormat;

    let material = new THREE.MeshBasicMaterial({ map: _VideoObject.value[0].texture});
    _VideoObject.value[0].body = new THREE.Mesh(_Plane, material);
    _VideoObject.value[0].body.position.set(_VideoObject.value[0].x, _VideoObject.value[0].y, _VideoObject.value[0].z);
    _VideoObject.value[0].body.rotation.set(_VideoObject.value[0].rx, _VideoObject.value[0].ry, _VideoObject.value[0].rz);
    _VideoObject.value[0].body.rotateX(_VideoObject.value[0].frx);
    _PanolensScene.add(_VideoObject.value[0].body);

    _VideoObject.value[0].texture.needsUpdate = true;
    _VideoObject.value[0].update = true;
}

function CreateVideo(_VideoObject, _Plane, _PanolensScene, _Loop, _Volume)
{
    _VideoObject.video = document.createElement( 'video' );
    _VideoObject.video.setAttribute("playsinline", "");
    _VideoObject.video.setAttribute("preload", "auto");
    _VideoObject.video.src = _VideoObject.track0;
    _VideoObject.video.load();
    _VideoObject.video.volume = _Volume;
    _VideoObject.video.loop = _Loop;

    _VideoObject.texture = new THREE.VideoTexture( _VideoObject.video );
    _VideoObject.texture.minFilter = THREE.LinearFilter;
    _VideoObject.texture.magFilter = THREE.LinearFilter;
    _VideoObject.texture.format = THREE.RGBFormat;

    let material = new THREE.MeshBasicMaterial({ map: _VideoObject.texture});
    _VideoObject.body = new THREE.Mesh(_Plane, material);
    _VideoObject.body.position.set(_VideoObject.x, _VideoObject.y, _VideoObject.z);
    _VideoObject.body.rotation.set(_VideoObject.rx, _VideoObject.ry, _VideoObject.rz);
    _VideoObject.body.rotateX(_VideoObject.frx);
    _PanolensScene.add(_VideoObject.body);

    _VideoObject.texture.needsUpdate = true;
    _VideoObject.update = true;
}

function ChangeVideo(_VideoObject, _NewVideoURL, _Loop)
{
    _VideoObject.value[0].video.src = _NewVideoURL;
    _VideoObject.value[0].video.load();
    _VideoObject.value[0].video.loop = _Loop;
    _VideoObject.value[0].texture.needsUpdate = true;
    PlayTelaoVideo();
}

//Add Sprite in Plane Texture
function AddSprite(_PanolensScene, _SpriteName, _SpriteDirectory, X, Y, Z, _RotationX, _RotationY, _RotationZ, _EspecialRotate, _Plane, _Tag = "", _Interaction, _NeedFocus,_NeedZoom = true, _PathAnalytics, _PathName)
{
    let texture = new THREE.TextureLoader().load(_SpriteDirectory.texture);
    let material = new THREE.MeshBasicMaterial({ map: texture, side: THREE.DoubleSide, transparent: true, alphaTest: 0.2, opacity: 0});
    if (isDebug){ loadedImages.push(_SpriteDirectory); lastImage = _SpriteDirectory;}
    _SpriteDirectory.body = new THREE.Mesh(_Plane, material);
    _SpriteDirectory.body.position.set(X, Y, Z);
    _SpriteDirectory.body.rotation.set((_RotationX * Math.PI /180), (_RotationY * Math.PI /180), (_RotationZ * Math.PI /180));
    _SpriteDirectory.body.rotateX(_EspecialRotate);
    _SpriteDirectory.body.userData.tag = _Tag;
    _PanolensScene.add(_SpriteDirectory.body);
    m_objects.push(_SpriteDirectory.body);

    if(_NeedFocus)
    {
        let linkInvisible = new PANOLENS.Infospot(1);
        linkInvisible.position.set(X, Y, Z);
        _PanolensScene.add(linkInvisible);
        _SpriteDirectory.body.addEventListener('click', function(){
            linkInvisible.focus( 1, TWEEN.Easing.Exponential.InOut );
            if(_NeedZoom)
            {
                DelayZoomIn();
            }
        });
    }

    if (_Interaction) {
        _SpriteDirectory.body.addEventListener('click', UnhideText.bind(this, _SpriteName, _PathAnalytics, _PathName));
    }

    // if(_Interaction)
    // {
    //     switch(_SpriteName)
    //     {

    //     }
    // }
}

function AddSpriteTransition(_PanolensScene, _SpriteName, _SceneToGo, _SpriteDirectory, X, Y, Z, _RotationX, _RotationY, _RotationZ, _EspecialRotate, _Plane, _Tag = "", _Interaction, _NeedFocus,_NeedZoom = true, _PathAnalytics, _PathName) {
    let texture = new THREE.TextureLoader().load(_SpriteDirectory.texture);
    let material = new THREE.MeshBasicMaterial({ map: texture, side: THREE.DoubleSide, transparent: true, alphaTest: 0.5, opacity: 0});
    if (isDebug){ loadedImages.push(_SpriteDirectory); lastImage = _SpriteDirectory;}
    _SpriteDirectory.body = new THREE.Mesh(_Plane, material);
    _SpriteDirectory.body.position.set(X, Y, Z);
    _SpriteDirectory.body.rotation.set(_RotationX, _RotationY, _RotationZ);
    _SpriteDirectory.body.rotateX(_EspecialRotate);
    _SpriteDirectory.body.userData.tag = "feedback";
    _PanolensScene.add(_SpriteDirectory.body);

    if(_NeedFocus)
    {
        let linkInvisible = new PANOLENS.Infospot(1);
        linkInvisible.position.set(X, Y, Z);
        _PanolensScene.add(linkInvisible);
        _SpriteDirectory.body.addEventListener('click', function(){
            linkInvisible.focus( 1, TWEEN.Easing.Exponential.InOut );
            if(true)
            {
                DelayZoomIn();
                GoToBubble(_SceneToGo);
            }
        });
    }

}

function AddSpriteCustom(_PanolensScene, _Material, _Name, X, Y, Z, _RotationX, _RotationY, _RotationZ, _EspecialRotate, _Plane, _Tag = "")
{
    let sprite;
    sprite = new THREE.Mesh(_Plane, _Material);
    sprite.position.set(X, Y, Z);
    sprite.rotation.set(_RotationX, _RotationY, _RotationZ);
    sprite.rotateX(_EspecialRotate);
    sprite.userData.tag = _Tag;
    sprite.name = _Name;
    m_objects.push(sprite);
    _PanolensScene.add(sprite);
}
if(iOS()){
    function SpriteUpdate(_name, _src, _width, _height) //Update a sprite's material
    {
      let objectFound = false;
      //let _object = getObjects();
      let _object = m_objects[0];
      objectFound = true;
    
      if (!objectFound)
      {
          panels_objects.push(_name);
          if (isDebug)
          {
            console.warn("Object was not found on object list iteration. Cancelling texture update...");
            console.warn(_name + " was added to panels_objects list.");
          }
          return null;
      }
    
      if (isDebug) console.log("Updating Texture...");
      //let tex = new THREE.ImageUtils.loadTexture(_src);
      const tex = new THREE.TextureLoader();
    
      tex.load(
          _src,
          function(texture)
          {
            texture.anisotropy = 16;
            texture.magFilter = THREE.NearestFilter;
            _object.material.map = texture;
            _object.material.needsUpdate = true;
            _object.visible = true;
            panels_objects.push(_name);
            if (isDebug) console.log(texture);
    
            // for (var i = 0; i < m_objects.length; i++)
            // {
            //     m_objects[i].visible = true;
            // }
            _object.visible = true;
    
          },
          undefined,
          function(err)
          {
              console.error("Error while updating sprite texture for: " + _name + " : " + err);
          }
      );
    }

}else{
    function SpriteUpdate(_name, _src, _width, _height) //Update a sprite's material
    {
      let objectFound = false;
      //let _object = getObjects();
      let _object = m_objects[0];
      objectFound = true;
    
      if (!objectFound)
      {
          panels_objects.push(_name);
          if (isDebug)
          {
            console.warn("Object was not found on object list iteration. Cancelling texture update...");
            console.warn(_name + " was added to panels_objects list.");
          }
          return null;
      }
    
      if (isDebug) console.log("Updating Texture...");
      //let tex = new THREE.ImageUtils.loadTexture(_src);
      const tex = new THREE.TextureLoader();
    
      tex.load(
          _src,
          function(texture)
          {
            texture.anisotropy = 16;
            texture.magFilter = THREE.NearestFilter;
            _object.material.map = texture;
            _object.material.needsUpdate = true;
            _object.visible = true;
            panels_objects.push(_name);
            if (isDebug) console.log(texture);
    
            for (var i = 0; i < m_objects.length; i++)
            {
                m_objects[i].visible = true;
            }
    
          },
          undefined,
          function(err)
          {
              console.error("Error while updating sprite texture for: " + _name + " : " + err);
          }
      );
    }
}
function StartExperience()
{
    Init(false, true);
    Animate();
}

var alertaShowing = false;
function UnhideText(_caseName, _PathAnalytics, _PathName)
{
    switch (_caseName) {
      case "agenda":
        $("#agendaModal").modal("show");
        CaptureAnalytics(_PathAnalytics, _PathName);
        TrackUser(login, _PathName);
      break;
      case "pesquisa":
        $("#pesquisaModal").modal("show");
        GetLink("pesquisa");
        // CaptureAnalytics(_PathAnalytics, _PathName);
        // TrackUser(login, _PathName);
      break;
      case "plenaria":
        // GetLink("plenariaGeral");
        $('#janssenModal').modal('show');
        $("#plenaria-btns").show();
        $("#salas-btns").hide();
        $("#btns-text").text("Plenária");
        $("#btns-sub-text").text("Reveja aqui, na íntegra, tudo o que aconteceu na plenária do evento. E nas aulas dos palestrantes internacionais.");
        CaptureAnalytics(_PathAnalytics, _PathName);
        TrackUser(login, _PathName);
      break;
      case "salas":
        $('#janssenModal').modal('show');
        $("#plenaria-btns").hide();
        $("#salas-btns").show();
        $("#btns-text").text("Clinical cases discussion");
        $("#btns-sub-text").text("Assista aqui as aulas exclusivas dos palestrantes nacionais.");
        CaptureAnalytics(_PathAnalytics, _PathName);
        TrackUser(login, _PathName);
      break;
      ///////////infos//////////
      case "internacional":
        $("#salaModal").modal("show");
        $("#salaTitulo").text("Palestrantes Internacionais");
        $("#internacional").show();
        $("#nacional").hide();
        CaptureAnalytics(_PathAnalytics, _PathName);
        TrackUser(login, _PathName);
      break;
      case "images":
        $("#modalCarouselTxtImg").modal("show");
        CaptureAnalytics(_PathAnalytics, _PathName);
        TrackUser(login, _PathName);
      break;
      case "pipeline":
        $("#modalCarouselPipeline").modal("show");  
        CaptureAnalytics(_PathAnalytics, _PathName);
        TrackUser(login, _PathName);
      break;
      case "nacional":
        $("#salaModal").modal("show");
        $("#salaTitulo").text("Palestrantes Nacionais");
        $("#nacional").show();
        $("#internacional").hide();
        CaptureAnalytics(_PathAnalytics, _PathName);
        TrackUser(login, _PathName);
      break;
    }
}

function ShowAlerta(){
    alertaShowing = true;
    $("#alerta").show();
    setTimeout(() => {
        $("#alerta").hide("slide",{ direction: "right" }, function(){
            alertaShowing = false;
        });
    }, 3000);
}
function ShowAlertaFechado(){
    alertaShowing = true;
    $("#fechado").show();
    setTimeout(() => {
        $("#fechado").hide("slide",{ direction: "right" }, function(){
            alertaShowing = false;
        });
    }, 3000);
}

function ShowZoom(_case){
    alertaShowing = true;
    GetLink(_case);
}

//Delay and Zoom In/Out Spot
function DelayZoomIn() {

    if (iOS) return;

    m_Viewer.OrbitControls.noZoom = true;
    m_Zoom = m_Viewer.getControl().object.fov;
    if (m_SpotInZoom == false) {
        setTimeout(() => {
            if (m_Zoom == m_controler.minFov) {
                m_SpotInZoom = true;
                return;
            }

            m_SpotInZoom = true;

            let delay = setInterval(function () {
                m_Zoom -= 0.5;
                m_Viewer.setCameraFov(m_Zoom);
                if (m_Zoom == m_controler.minFov) {
                    clearInterval(delay);
                    m_Zoom = m_controler.minFov;
                }
            }, 10);
        }, 700);
    }

    m_SpotInZoom = true;
}

function DelayZoomOut() {
    m_Viewer.OrbitControls.noZoom = false;
    m_Zoom = m_Viewer.getControl().object.fov;
    if (m_SpotInZoom) {
        if (m_Zoom == m_controler.maxFov) {
            m_SpotInZoom = false;
            return;
        }

        m_SpotInZoom = false;

        let delay = setInterval(function () {
            m_Zoom += 1;
            m_Viewer.setCameraFov(m_Zoom);
            if (m_Zoom == m_controler.maxFov) {
                clearInterval(delay);
                m_Zoom = m_controler.maxFov;
            }
        }, 10);
    }
}

function ZoomIn()
{
    m_Zoom = m_Viewer.getControl().object.fov;
    if(m_SpotInZoom == false)
    {
        if(m_Zoom == 40)
        {
            m_SpotInZoom = false;
            return;
        }

        let delay = setInterval(function()
        {
            m_Zoom -= 0.5;
            m_Viewer.setCameraFov(m_Zoom);
            if(m_Zoom == 40){
                clearInterval(delay);
                m_Zoom = 40;
            }
        }, 10);
    }

    m_SpotInZoom = true;
}

//Create 2D Text
function Create2DText(_TextObject, _PanolensScene, _Plane, _Tag = "")
{
    _TextObject.canvas = document.createElement('canvas').getContext('2d');
    let textMetrics = _TextObject.canvas.measureText(_TextObject.text);
    _TextObject.canvas.canvas.width = _TextObject.textWidth + textMetrics.width;
    _TextObject.canvas.canvas.height = _TextObject.textHeight;
    _TextObject.canvas.font = _TextObject.fontWeight + " " + _TextObject.textSize + "px " + _TextObject.font;
    _TextObject.canvas.textAlign = "center";
    _TextObject.canvas.textBaseline = "middle";
    _TextObject.canvas.fillStyle = _TextObject.textColor;
    _TextObject.canvas.fillText(_TextObject.text, _TextObject.canvas.canvas.width/2, _TextObject.canvas.canvas.height/2);

    _TextObject.texture = new THREE.CanvasTexture(_TextObject.canvas.canvas);
    _TextObject.texture.minFilter = THREE.LinearFilter;
    _TextObject.texture.magFilter = THREE.LinearFilter;
    _TextObject.texture.format = THREE.RGBAFormat;
    _TextObject.texture.needsUpdate = true;

    const material = new THREE.MeshBasicMaterial({map: _TextObject.texture, transparent: true, alphaTest: 0.5, opacity: 1});

    _TextObject.body = new THREE.Mesh(_Plane, material);
    _TextObject.body.userData.tag = _Tag;
    _TextObject.body.position.set(_TextObject.x, _TextObject.y, _TextObject.z);
    _TextObject.body.rotation.set(_TextObject.rx, _TextObject.ry, _TextObject.rz);
    _TextObject.body.rotateX( _TextObject.frx);
    _TextObject.body.rotateY( _TextObject.fry);
    _TextObject.body.name = _TextObject
    m_objects.push(_TextObject.body);
    _PanolensScene.add(_TextObject.body);
}

function UpdateText(_Text, _TextObject)
{
    // console.log(_Text)
    _TextObject.texture.needsUpdate = true;
    _TextObject.canvas.clearRect(0, 0, _TextObject.canvas.canvas.width, _TextObject.canvas.canvas.height);
    _TextObject.canvas.fillText(_Text, _TextObject.canvas.canvas.width/2, _TextObject.canvas.canvas.height/2);
}

function AdjustClockText(_hours, _minutes, _seconds)
{
    //TINHA ESPAÇO em cada ""
    let hours = _hours;
    let minutes = _minutes;
    let seconds = _seconds;

    if(hours.toString().includes("1"))
    {
        let temp = [hours.toString().slice(0,1), hours.toString().slice(1)];

        if(temp[0].includes("1") && temp[1].includes("1"))
            hours = "" + hours.toString().slice(0,1) + "" + hours.toString().slice(1);
        else if(hours.toString().indexOf("1") == 0)
            hours = "" + hours.toString().slice(0,1) + hours.toString().slice(1);
        else if(hours.toString().indexOf("1") == 1)
            hours = hours.toString().slice(0,1) + "" + hours.toString().slice(1);
        else
            hours = hours;
    }

    
    if(minutes.toString().includes("1"))
    {
        let temp = [minutes.toString().slice(0,1), minutes.toString().slice(1)];

        if(temp[0].includes("1") && temp[1].includes("1"))
            minutes = "" + minutes.toString().slice(0,1) + "" + minutes.toString().slice(1);
        else if(minutes.toString().indexOf("1") == 0)
            minutes = "" + minutes.toString().slice(0,1) + minutes.toString().slice(1);
        else if(minutes.toString().indexOf("1") == 1)
            minutes = minutes.toString().slice(0,1) + "" + minutes.toString().slice(1);
        else
            minutes = minutes;
    }

    
    if(seconds.toString().includes("1"))
    {
        let temp = [seconds.toString().slice(0,1), seconds.toString().slice(1)];

        if(temp[0].includes("1") && temp[1].includes("1"))
            seconds = "" + seconds.toString().slice(0,1) + "" + seconds.toString().slice(1);
        else if(seconds.toString().indexOf("1") == 0)
            seconds = "" + seconds.toString().slice(0,1) + seconds.toString().slice(1);
        else if(seconds.toString().indexOf("1") == 1)
            seconds = seconds.toString().slice(0,1) + "" + seconds.toString().slice(1);
        else
            seconds = seconds;
    }

    let temp = [hours, minutes, seconds];
    return temp;
}

function ShowClocks(_Value)
{
    m_UpdateTimerStart = _Value;
    
    
    // for(let i = 0; i < m_ClockFeedbackTextObject.value.length; i++)
    // {
    //     m_ClockFeedbackTextObject.value[i].body.visible = _Value;
    // }
    
    // for(let i = 0; i < m_ClockTimerTextObject.value.length; i++)
    // {
    //     m_ClockTimerTextObject.value[i].body.visible = _Value;
    // }
}

function AddCanvas(_CanvasObject, _PanolensScene, _Plane)
{
    _CanvasObject.value[0].canvas = document.createElement('canvas').getContext('2d');

    _CanvasObject.value[0].canvas.canvas.width = _CanvasObject.value[0].width;
    _CanvasObject.value[0].canvas.canvas.height = _CanvasObject.value[0].height;

    _CanvasObject.value[0].canvas.fillStyle = _CanvasObject.value[0].color;
    _CanvasObject.value[0].canvas.fillRect(0, 0, _CanvasObject.value[0].width, _CanvasObject.value[0].height);

    _CanvasObject.value[0].texture = new THREE.CanvasTexture(_CanvasObject.value[0].canvas.canvas);
    _CanvasObject.value[0].texture.minFilter = THREE.LinearFilter;
    _CanvasObject.value[0].texture.magFilter = THREE.LinearFilter;
    _CanvasObject.value[0].texture.format = THREE.RGBAFormat;
    _CanvasObject.value[0].texture.needsUpdate = true;

    const material = new THREE.MeshBasicMaterial({map: _CanvasObject.value[0].texture, transparent: true});

    _CanvasObject.value[0].body = new THREE.Mesh(_Plane, material);
    _CanvasObject.value[0].body.position.set(_CanvasObject.value[0].x, _CanvasObject.value[0].y, _CanvasObject.value[0].z);
    _CanvasObject.value[0].body.rotation.set(_CanvasObject.value[0].rx, _CanvasObject.value[0].ry, _CanvasObject.value[0].rz);
    _CanvasObject.value[0].body.rotateX( _CanvasObject.value[0].frx);
    _PanolensScene.add(_CanvasObject.value[0].body);
}

function CreateCanvas(_CanvasObject, _PanolensScene, _Plane)
{
    _CanvasObject.canvas = document.createElement('canvas').getContext('2d');

    _CanvasObject.canvas.canvas.width = _CanvasObject.width;
    _CanvasObject.canvas.canvas.height = _CanvasObject.height;

    _CanvasObject.canvas.fillStyle = _CanvasObject.color;
    _CanvasObject.canvas.fillRect(0, 0, _CanvasObject.width, _CanvasObject.height);

    _CanvasObject.texture = new THREE.CanvasTexture(_CanvasObject.canvas.canvas);
    _CanvasObject.texture.minFilter = THREE.LinearFilter;
    _CanvasObject.texture.magFilter = THREE.LinearFilter;
    _CanvasObject.texture.format = THREE.RGBAFormat;
    _CanvasObject.texture.needsUpdate = true;

    const material = new THREE.MeshBasicMaterial({map: _CanvasObject.texture, transparent: true});

    _CanvasObject.body = new THREE.Mesh(_Plane, material);
    _CanvasObject.body.position.set(_CanvasObject.x, _CanvasObject.y, _CanvasObject.z);
    _CanvasObject.body.rotation.set(_CanvasObject.rx, _CanvasObject.ry, _CanvasObject.rz);
    _CanvasObject.body.rotateX( _CanvasObject.frx);
    _PanolensScene.add(_CanvasObject.body);
}

function UpdateCanvasColor(_CanvasObject, _Color)
{
    _CanvasObject.value[0].texture.needsUpdate = true;
    _CanvasObject.value[0].canvas.clearRect(0, 0, _CanvasObject.value[0].width, _CanvasObject.value[0].height);
    _CanvasObject.value[0].canvas.fillStyle = _Color;
    _CanvasObject.value[0].canvas.fillRect(0, 0, _CanvasObject.value[0].width, _CanvasObject.value[0].height);
}

function UpdateCanvasTexture(_CanvasObject, _Color)
{
    _CanvasObject.texture.needsUpdate = true;
    _CanvasObject.canvas.clearRect(0, 0, _CanvasObject.width, _CanvasObject.height);
    _CanvasObject.canvas.fillStyle = _Color;
    _CanvasObject.canvas.fillRect(0, 0, _CanvasObject.width, _CanvasObject.height);
}



function OpenModal(_url) {
    $("#btnChat").show();
    $("#btnIdioma").show();
    /* $("#chat").show(); */
    $("#idioma").show();
    $("#wppBtn").hide();
    $("#videoModal").modal('show');
    // console.log(_url);
    if (navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i) || navigator.userAgent.match(/BlackBerry/i) || navigator.userAgent.match(/Windows Phone/i)) {
        $('#iframeModal').attr('src', _url+ "?autoplay=0&loop=0&autopause=0&playsinline=1&portrait=0&title=0&fullscreen=0");
    } else {
        $('#iframeModal').attr('src', _url+ "?autoplay=0&loop=0&autopause=0&playsinline=1&portrait=0&title=0&fullscreen=0");
    }
    if (iOS()) {
        $('#iframeModal').attr('src', _url + "?autoplay=0&loop=0&autopause=0&playsinline=1&portrait=0&title=0&fullscreen=0&muted=1");
    }
    $('#iframeModal').on("load", function () {
        $("#loaderStart").hide();
        m_VimeoLive = new Vimeo.Player($('#iframeModal'));
        setTimeout(() => {
            m_VimeoLive.play();
        }, 200);
    });
}
function OpenModalSpecific(_url) {
    $("#btnChat").show();
    $("#btnIdioma").show();
    /* $("#chat").show(); */
    $("#idioma").show();
    $("#wppBtn").hide();
    $("#videoModalPlenaria").modal('show');
    // console.log(_url);
    if (navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i) || navigator.userAgent.match(/BlackBerry/i) || navigator.userAgent.match(/Windows Phone/i)) {
        $('#iframeModalPlenaria').attr('src', _url+ "?autoplay=0&loop=0&autopause=0&playsinline=1&portrait=0&title=0&fullscreen=0");
    } else {
        $('#iframeModalPlenaria').attr('src', _url+ "?autoplay=0&loop=0&autopause=0&playsinline=1&portrait=0&title=0&fullscreen=0");
    }
    if (iOS()) {
        $('#iframeModalPlenaria').attr('src', _url + "?autoplay=0&loop=0&autopause=0&playsinline=1&portrait=0&title=0&fullscreen=0&muted=1");
    }
    $('#iframeModalPlenaria').on("load", function () {
        $("#loaderStart").hide();
        m_VimeoLive = new Vimeo.Player($('#iframeModalPlenaria'));
        setTimeout(() => {
            m_VimeoLive.play();
        }, 200);
    });
}


function PlayModalVideo()
{
    $("#modal360").modal("show");
    $("#loaderStart").show();
    OpenModal(m_linkVideo);
}

function OpenNewWindow(_link)
{
    window.open(_link);
}

