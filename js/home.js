var login;
var username;
var on;
var access;

window.onfocus = function () {
    // CheckDate("home");
}

$(function () {
        login = getUrlVars()["l"];
        on = getUrlVars()["on"];

        if (login == null || on == null) {
            //console.log("login ou senha é nulo");
            location.href = 'index.html';
        } else {
            // GetLiveLink();
            CheckUserOnline(login, on);
        }
});
GetLink("links");


// GetLiveLink("vimeo");

function ButtonClick(_case, _url){
	switch (_case) {
        //////////////////////// PESQUISAS /////////////////////////
		case "ChatOpen":
            $("#btnIdioma").hide();
			$("#btnChat").hide();
			$("#chatContainer").show(function(){
				$("#chatIframe").fadeIn();
			});
            CaptureAnalytics("/home/chat/abriu", "Abriu o chat");
            TrackUser(login, "Abriu o chat");
		break;
		case "ChatClose":
			$("#chatIframe").fadeOut(function(){
				$("#chatContainer").hide();
                $("#btnChat").show();
                $("#btnIdioma").show();
			});
            CaptureAnalytics("/home/chat/fechou", "Fechou o chat");
            TrackUser(login, "Fechou o chat");
		break;
		case "ChatCloseEspecial":
			$("#chatIframe").fadeOut(function(){
				$("#chatContainer").hide();
                $("#btnChat").hide();
                $("#btnIdioma").hide();
			});
		break;
		case "idiomaOpen":
            $("#btnIdioma").hide();
			$("#btnChat").hide();
			$("#idiomaContainer").show(function(){
				$("#idiomaInside").fadeIn();
			});
		break;
		case "idiomaClose":
			$("#idiomaInside").fadeOut(function(){
				$("#idiomaContainer").hide();
                $("#btnIdioma").show();
                $("#btnChat").show();
			});
		break;
		case "idiomaCloseEspecial":
			$("#idiomaInside").fadeOut(function(){
				$("#idiomaContainer").hide();
                $("#btnIdioma").hide();
                $("#btnChat").hide();
			});
		break;
		case "pesquisa":
			window.open(_url, "_blank");
            CaptureAnalytics("Home/Pesquisa/Abriu", "Abriu o link da Pesquisa");
            TrackUser(login, "Abriu o link da Pesquisa");
		break;
		// case "plenaria":
        //     OpenModal(_url);
        //     $("#plenariaModal").modal("hide");
        //     CaptureAnalytics("Home/Plenaria/Abriu", "Abriu video da Plenaria");
        //     TrackUser(login, "Abriu video da Plenaria");
		// break;
		case "Endoscopia":
			window.open(_url, "_blank");
            CaptureAnalytics("Home/Informação/Carousel Banners/" + _case, "Abriu o link " + _case + " do Carousel");
            TrackUser(login, "Abriu o link " + _case + " do Carousel");
		break;
		case "Medical":
			window.open(_url, "_blank");
            CaptureAnalytics("Home/Informação/Carousel Banners/" + _case, "Abriu o link " + _case + " do Carousel");
            TrackUser(login, "Abriu o link " + _case + " do Carousel");
		break;
		case "IDB":
			window.open(_url, "_blank");
            CaptureAnalytics("Home/Informação/Carousel Banners/" + _case, "Abriu o link " + _case + " do Carousel");
            TrackUser(login, "Abriu o link " + _case + " do Carousel");
		break;
		case "DDW":
			window.open(_url, "_blank");
            CaptureAnalytics("Home/Informação/Carousel Banners/" + _case, "Abriu o link " + _case + " do Carousel");
            TrackUser(login, "Abriu o link " + _case + " do Carousel");
		break;
		case "Global":
			window.open(_url, "_blank");
            CaptureAnalytics("Home/Informação/Carousel Banners/" + _case, "Abriu o link " + _case + " do Carousel");
            TrackUser(login, "Abriu o link " + _case + " do Carousel");
		break;
		case "Pro":
			window.open(_url, "_blank");
            CaptureAnalytics("Home/Informação/Carousel Banners/" + _case, "Abriu o link " + _case + " do Carousel");
            TrackUser(login, "Abriu o link " + _case + " do Carousel");
		break;
		case "Controversias":
			window.open(_url, "_blank");
            CaptureAnalytics("Home/Informação/Carousel Banners/" + _case, "Abriu o link " + _case + " do Carousel");
            TrackUser(login, "Abriu o link " + _case + " do Carousel");
		break;
		case "Vacina":
			window.open(_url, "_blank");
            CaptureAnalytics("Home/Informação/Carousel Banners/" + _case, "Abriu o link " + _case + " do Carousel");
            TrackUser(login, "Abriu o link " + _case + " do Carousel");
		break;
		case "wpp":
			window.open("https://api.whatsapp.com/send?phone=+5511981296796", "_blank");
            CaptureAnalytics("Home/WhatsApp", "Abriu o link do WhatsApp");
            TrackUser(login, "Abriu o link do WhatsApp");
		break;
		case "sala1":
			GetLink(_case);
			$('#janssenModal').modal('hide');
			CaptureAnalytics("Home/Salas/Sala 1/Abriu", "Abriu video sala 1 das Salas");
			TrackUser(login, "Abriu video Sala 1 das Salas");
		break;
		case "sala2":
			GetLink(_case);
			$('#janssenModal').modal('hide');
			CaptureAnalytics("Home/Salas/Sala 2/Abriu", "Abriu video sala 2 das Salas");
			TrackUser(login, "Abriu video Sala 2 das Salas");
		break;
		case "sala3":
			GetLink(_case);
			$('#janssenModal').modal('hide');
			CaptureAnalytics("Home/Salas/Sala 3/Abriu", "Abriu video sala 3 das Salas");
			TrackUser(login, "Abriu video Sala 3 das Salas");
		break;
		case "plenaria":
			GetLink(_case);
			$('#janssenModal').modal('hide');
			CaptureAnalytics("Home/Plenaria/Plenaria/Abriu", "Abriu video da Plenaria");
			TrackUser(login, "Abriu video da Plenaria");
		break;
		case "plenariasala1":
			GetLink(_case);
			$('#janssenModal').modal('hide');
			CaptureAnalytics("Home/Plenaria/Sala 1/Abriu", "Abriu video sala 1 da Plenaria");
			TrackUser(login, "Abriu video Sala 1 da Plenaria");
		break;
		case "plenariasala2":
			GetLink(_case);
			$('#janssenModal').modal('hide');
			CaptureAnalytics("Home/Plenaria/Sala 2/Abriu", "Abriu video sala 2 da Plenaria");
			TrackUser(login, "Abriu video Sala 2 da Plenaria");
		break;
		case "plenariasala3":
			GetLink(_case);
			$('#janssenModal').modal('hide');
			CaptureAnalytics("Home/Plenaria/Sala 3/Abriu", "Abriu video sala 3 da Plenaria");
			TrackUser(login, "Abriu video Sala 3 da Plenaria");
		break;
    }
}

$(function(){
    CheckDate("cronoT");
});
   
window.onfocus = function () {
    CheckDate("cronoT");
}


var videoLinkPT;
var videoLinkEN;
var langVideo = "pt";
var vimeoLang = "pt";
function ChangeVideo(){
	switch (langVideo) {
		case "pt":
			if(vimeoLang == "pt") return;
			else{
				vimeoLang = "pt";
				document.getElementById("iframeModal").src =videoLinkPT + "?autoplay=0&loop=0&autopause=0&controls=1&playsinline=1&portrait=0&title=0&fullscreen=0";
				
				$('#iframeModal').on("load", function () {
					m_VimeoLive = new Vimeo.Player($('#iframeModal'));
					m_VimeoLive.play();
				});
				CaptureAnalytics('/video/idioma/pt','Trocou o idioma para português');
				TrackUser(login, "Trocou o idioma para português");
			}
		break;
		case "en":
			if(vimeoLang == "en") return;
			else{
				vimeoLang = "en";
				document.getElementById("iframeModal").src = videoLinkEN + "?autoplay=0&loop=0&autopause=0&controls=1&playsinline=1&portrait=0&title=0&fullscreen=0";
				
				$('#iframeModal').on("load", function () {
					m_VimeoLive = new Vimeo.Player($('#iframeModal'));
					m_VimeoLive.play();
				});
				CaptureAnalytics('/video/idioma/en','Trocou o idioma para inglês');
				TrackUser(login, "Trocou o idioma para inglês");
			}
		break;
	}
}
function selectIdioma(idioma) {
    langVideo = idioma;
	console.log(langVideo)
}