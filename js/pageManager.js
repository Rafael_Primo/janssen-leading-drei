
// Init(true, true);
// Animate();
//  CheckDate();
StartExperience();


////////////////////////////////////////////INITIALIZE PAGE////////////////////////////////////////////
function Init(_EnableDebug, _EnablePageLoader)
{
    m_Clock = new THREE.Clock();
    m_PanolensScene = [];
    m_PanolensSceneTextureDirectory = [];
    m_InfoSpot = [];
    m_PanoloensLoaded = false;
    m_Zoom = 60;
    m_SpotInZoom = false;
    m_ClockUpdate = 0;
    m_UpdateTimerStart = false;

    // console.log(m_IsAfternoon);
    var x = setInterval(function () {
        m_now[0] = m_now[0] - 1000;
        m_now[1] = m_now[1] - 1000;
        m_now[2] = m_now[2] - 1000;
        m_now[3] = m_now[3] - 1000;
        m_now[4] = m_now[4] - 1000;
        // m_now[5] = m_now[5] - 1000;
    }, 1000);

    if(iOS())
    {
        m_PanolensSceneTextureDirectory[0] = 'textures/BOLHA_01.jpg';
        m_PanolensSceneTextureDirectory[1] = 'textures/BOLHA_01.jpg';
    }
    else
    {
        m_PanolensSceneTextureDirectory[0] = 'textures/BOLHA_01.jpg';
        m_PanolensSceneTextureDirectory[1] = 'textures/BOLHA_01.jpg';
    }

    if(_EnableDebug)
    {
        m_Viewer = new PANOLENS.Viewer({output: 'console', controlBar: true, controlButtons: []});
    }
    else
    {
        m_Viewer = new PANOLENS.Viewer({controlBar: true, controlButtons: []});
    }

    for(let i = 0; i < m_PanolensSceneTextureDirectory.length; i++)
    {
        m_PanolensScene[i] = new PANOLENS.ImagePanorama(m_PanolensSceneTextureDirectory[i]);
        m_Viewer.add(m_PanolensScene[i]);
    }
    // How far you can orbit vertically, upper and lower limits.Range is 0 to Math.PI radians.
    // m_Viewer.OrbitControls.minPolarAngle = Math.PI / 3;
    // m_Viewer.OrbitControls.maxPolarAngle = Math.PI * 2 / 3;
    
    // How far you can orbit horizontally, upper and lower limits. If set, must be a sub-interval of the interval [ - Math.PI, Math.PI ].
    // m_Viewer.OrbitControls.minAzimuthAngle = - Math.PI / 3;
    // m_Viewer.OrbitControls.maxAzimuthAngle = Math.PI / 3;

    m_controler = m_Viewer.getControl();
    m_controler.maxFov = 70;
    m_controler.minFov = 40;
    m_Zoom = m_controler.maxFov;

    m_Viewer.tweenControlCenter( new THREE.Vector3(5000.00,-1864.30,58.76),0);
    m_Viewer.OrbitControls.momentumDampingFactor = 0.8;


    InstanceMethods();
    LoaderPage(_EnablePageLoader);
}
// var dragControls;
// dragControls = new THREE.DragControls(m_objects, m_Viewer.camera, m_Viewer.getRenderer().domElement);
// dragControls.addEventListener( 'dragstart', function ( event ) {
//     m_Viewer.disableControl();
// });
// dragControls.addEventListener ( 'drag', function( event ){
//     if(event.object.position.x >= 5000) event.object.position.x = 5000;
//     if(event.object.position.y >= 5000) event.object.position.y = 5000;
//     if(event.object.position.z >= 5000) event.object.position.z = 5000;
//     if(event.object.position.x <= -5000) event.object.position.x = -5000
//     if(event.object.position.y <= -5000) event.object.position.y = -5000;
//     if(event.object.position.z <= -5000) event.object.position.z = -5000;;
// })
// dragControls.addEventListener( 'dragend', function ( event ) {
//     console.warn("Sua posição é: " + event.object.position.x.toFixed(1) + ", " + event.object.position.y.toFixed(1) + ", " + event.object.position.z.toFixed(1));
//     m_Viewer.enableControl();
// });
// dragControls.enabled = false;
// window.addEventListener( 'keyup', function ( event ) {

//     switch ( event.key ) {

//         case "Control" && "Shift": // Shift
//             dragControls.enabled = true;
//             break;
//         case "Escape":
//             dragControls.enabled = false;
//             break;
//     }
// });


////////////////////////////////////////////RENDER AND UPDATE////////////////////////////////////////////
var updateText = false;
setTimeout(() => {
    updateText = true;
}, 2000);
function Animate()
{
    requestAnimationFrame(Animate);
    // if(m_now[2] < 0 && m_IsAfternoon == false){
    //     m_IsAfternoon = true;
    // }
    if(updateText == false){
        UpdateText("EM BREVE", m_ClockFeedbackTextObject.value[0]);
        UpdateText("EM BREVE", m_ClockFeedbackTextObject.value[1]);
    }
    if(iOS())
    {
        switch (m_ActualScene) {
            case 0:
                if(m_UpdateTimerStart)
                {
                    UpdateTimer();
                }
            break;
            case 1:
            break;

            case 2:
            break;

            default:
            break;
        }
    }
    else
    {
        switch (m_ActualScene) {
            case 0:
                if(m_UpdateTimerStart)
                {
                    UpdateTimer();
                }
            break;
            case 1:
            break;

            case 2:
            break;

            default:
            break;
        }
    }
}

function UpdateTimer()
{
    if(m_ClockFeedbackTextObject.value[0].canvas != null)
    {
        m_ClockUpdate++;

        if(m_ClockUpdate > 10)
        {
            //////////////////////// TIMER ////////////////////////
            m_ClockUpdate = 0;

            let distance1 = m_now[0];
            let distance2 = m_now[1];
            let distance3 = m_now[2];
            let distance4 = m_now[3];
            let distance5 = m_now[4];

            if (distance1 < 0 && distance2 > 0 && distance3 > 0)
            {
                if(m_PlenariaOpen == false && m_PlenariaFinished == false){
                    m_ClockFeedbackTextObject.value[0].body.visible = false;
                    m_SpriteObjects.value[5].body.visible = true;
                    m_PlenariaOpen = true;
                    $("#plenariaModal").modal("show");
                    GetLink("plenaria");
                }
            }
            else if(distance1 < 0 && distance2 < 0 && distance3 > 0){
                // m_PlenariaFinished = true;
                if(!m_RoomShowed){
                    m_ClockFeedbackTextObject.value[0].body.visible = false;
                    m_ClockFeedbackTextObject.value[1].body.visible = false;
                    m_SpriteObjects.value[5].body.visible = true;
                    m_SpriteObjects.value[6].body.visible = true;
                    $("#textPlenaria2").attr("onclick", "$('#janssenModal').modal('show');$('#plenariaModal2').modal('hide');");

                    if(!$("#plenariaModal").is(":visible") && !m_RoomShowed){
                        $("#plenariaModal2").modal("show");
                        m_RoomShowed = true;
                    }
                }
            }
            else if(distance3 < 0 && distance4 > 0 && distance5 > 0){
                if(!$("#plenariaModal").is(":visible") && !m_RoomPopShowed){
                    m_RoomPopShowed = true;
                    $("#plenariaModal2").modal("show");
                    if(userGroup == 4){
                        $("#textPlenaria2").attr("onclick", "$('#janssenModal').modal('show');$('#plenariaModal2').modal('hide');");
                    } else {
                        $("#textPlenaria2").attr("onclick", "GetLink('sala" + userGroup + "')");
                    }
                    m_ClockFeedbackTextObject.value[0].body.visible = false;
                    m_ClockFeedbackTextObject.value[1].body.visible = false;
                    m_SpriteObjects.value[5].body.visible = true;
                    m_SpriteObjects.value[6].body.visible = true;
                }
            }
            else if(distance4 < 0 && distance5 > 0){
                if(m_PlenariaAfterOpen == false && m_PlenariaFinished == false){
                    m_ClockFeedbackTextObject.value[0].body.visible = false;
                    m_ClockFeedbackTextObject.value[1].body.visible = false;
                    m_SpriteObjects.value[5].body.visible = true;
                    m_SpriteObjects.value[6].body.visible = true;
                    m_PlenariaAfterOpen = true;
                    $("#plenariaModal").modal("show");
                    $("#plenariaText").text("Por favor, dirija-se à plenária.");
                    GetLink("plenaria");
                }
            }
            else if(distance4 < 0 && distance5 < 0 && m_SatisfactionShow == false){
                m_ClockFeedbackTextObject.value[0].body.visible = false;
                m_ClockFeedbackTextObject.value[1].body.visible = false;
                m_SpriteObjects.value[5].body.visible = true;
                m_SpriteObjects.value[6].body.visible = true;
                m_SatisfactionShow = true;
                $("#pesquisaModal").modal("show");
                GetLink("pesquisa");
            }
            else
            {
                let days = Math.floor(distance1 / (1000 * 60 * 60 * 24));
                let hours = Math.floor((distance1 % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                let minutes = Math.floor((distance1 % (1000 * 60 * 60)) / (1000 * 60));
                let seconds = Math.floor((distance1 % (1000 * 60)) / 1000);

                hours = hours + (days * 24);

                if (hours.toString().length == 1)
                    hours = "0" + hours;
                if (minutes.toString().length == 1)
                    minutes = "0" + minutes;
                if (seconds.toString().length == 1)
                    seconds = "0" + seconds;

                let temp = AdjustClockText(hours, minutes, seconds);
                textHourAndMinutes = (temp[0] + ":" + temp[1]);
                textSeconds = (":" + temp[2]);

                //m_ClockTimerTextObject.value[0].body.visible = true;
                // m_ClockFeedbackTextObject.value[0].body.visible = false;

                m_ClockUpdate = 0;
                //UpdateText((textHourAndMinutes + textSeconds), m_ClockTimerTextObject.value[0]);

            }
        }
    }
}

////////////////////////////////////////////Instanciate Methods////////////////////////////////////////////
function InstanceMethods()
{
    LinkAndEvents();
    AddSpecificSpots();
    AddLinkSpots();
    AddSprites();
    AddText2DTimer()

    if(iOS())
    {
        PlaceObjectsInScene();
    }
    else
    {
        PlaceObjectsInScene();
    }
    $('.panolens-container').attr("id", "pano");
    document.getElementById("pano").addEventListener( 'mousemove', onDocumentMouseMove, false );
}


function PlaceObjectsInScene()
{

}

////////////////////////////////////////////Create Objects////////////////////////////////////////////
function AddText2DTimer()
{
    let plane = CreatePlane(1000, 300, 1, 1);

    // Create2DText(m_ClockTimerTextObject.value[0], m_PanolensScene[0], plane, "");
    Create2DText(m_ClockFeedbackTextObject.value[0], m_PanolensScene[0], plane, "");
    Create2DText(m_ClockFeedbackTextObject.value[1], m_PanolensScene[0], plane, "");
    // m_ClockTimerTextObject.value[0].body.visible = false;
    // m_ClockFeedbackTextObject.value[0].body.visible = false;
}

function AddSprites() {
    if (!iOS())
    {

    }
    let plane = CreatePlane(800, 525, 1, 1);
    let planeFar = CreatePlane(700, 460, 1, 1);
    let planeFurther = CreatePlane(650, 426, 1, 1);
    let planeDoors = CreatePlane(1350, 1050, 1, 1);
    let planeDoors2 = CreatePlane(1600, 800, 1, 1);

    //TV BOLHA 1 (DIREITA PRA ESQUERDA)
    
    AddSprite(m_PanolensScene[0],"internacional", m_SpriteObjects.value[0], 3700, 975, -4000, 0, -25, 0, 0, plane, "feedback", true, true, false, "Home/Informação/Internacional", "Acessou a Informação Internacional");
    AddSprite(m_PanolensScene[0],"nacional", m_SpriteObjects.value[1], 4350, 950, -3500, 0, -25, 0, 0,  plane, "feedback", true, true, false, "Home/Informação/Nacional", "Acessou a Informação Nacional");
    AddSprite(m_PanolensScene[0],"images", m_SpriteObjects.value[2], 3000, 1050, -4500, 0, -25, 0, 0, plane, "feedback", true, true, false, "Home/Informação/Carrousel Banners", "Acessou a Informação de Carrousel Banners");
    AddSprite(m_PanolensScene[0],"pipeline", m_SpriteObjects.value[3], 4610,851, -2778, 0, -30, 0, 0,  planeFar, "feedback", true, true, false, "Home/Informação/Carrousel Pipeline", "Acessou a Informação de Carrousel Pipeline");
    AddSprite(m_PanolensScene[0],"agenda", m_SpriteObjects.value[4], 4791.0, 774.7, -2104.0, 0, -45, 0, 0, planeFurther, "feedback", true, true, false, "Home/Agenda/Acessou", "Acessou a Agenda");
    // AddSprite(m_PanolensScene[0],"pesquisa", m_SpriteObjects.value[4], 4541.7, 730.4, -1990.4, 0, -35, 0, 0, planeFar, "feedback", true, true, false, "Home/Pesquisa/Acessou", "Acessou a Pesquisa");
    
    AddSprite(m_PanolensScene[0],"plenaria", m_SpriteObjects.value[5], 3030.5, 147.4, 4325.7, 0, 10, 0, 0, planeDoors, "feedback", true, true, false, "Home/Plenaria/Modal", "Acessou o modal Plenaria");
    AddSprite(m_PanolensScene[0],"salas", m_SpriteObjects.value[6], 4485.5, 147.4, 3501.3, 0, 30, 0, 0, planeDoors, "feedback", true, true, false, "Home/Salas/Modal", "Acessou o modal Salas");
    
    m_SpriteObjects.value[5].body.visible = false;
    m_SpriteObjects.value[6].body.visible = false;
}

//Change sprites on runtime
function AddSpriteSheets() {
    // let planePalco = CreatePlane(1550, 3000, 1, 1);
}



function AddLinkSpots()
{
    // CreateLinkSpot(m_PanolensScene[0], 'imgs/infospot.png', "feedback", 500, 2662.45, -1561.18, -5000.00, m_PanolensScene[1], true, 2075.69, -33.22, -5000.00);
}


function AddSpecificSpots()
{
  //Bolha 1 -
  //CreateSpecificSpot(m_PanolensScene[0], 'imgs/infospot.png', "feedback", 300,  5000.00, 192.47,  1604.53, 12, true, "placeholder","", false, "Bolha 01 - Placeholder", "Placeholder");
}
