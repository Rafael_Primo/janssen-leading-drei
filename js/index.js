function GetFieldsData()
{
    let email = $("#email").val();
    let name = $("#nome").val();
    let surname = $("#sobrenome").val();
    let crm = $("#crm").val();
    let uf = $("#uf").val();
    email = email.replace(/^\s+/, '').replace(/\s+$/, '');
    name = name.replace(/^\s+/, '').replace(/\s+$/, '');
    surname = surname.replace(/^\s+/, '').replace(/\s+$/, '');
    crm = crm.replace(/^\s+/, '').replace(/\s+$/, '');
    uf = uf.replace(/^\s+/, '').replace(/\s+$/, '');

    if(email === "" || name === "" || surname === "" || crm === "" || uf === "")
    {
        // console.log("só espaços brancos");
        $('#feedback').show();
        $('#feedback').text("Preencha todos os campos");
    }
    else
    {
        // console.log("tudo preenchido");
        let tEmail = email.replace(/ +(?= )/g,'');
        let tName = name.replace(/ +(?= )/g,'');
        let tSurname = surname.replace(/ +(?= )/g,'');
        let tCrm = crm.replace(/ +(?= )/g,'');
        let tUf = uf.replace(/ +(?= )/g,'');
        if(ValidateText(tName)) 
        {
            $('#feedback').show();
            $('#feedback').text("O campo de nome contém caracteres inválidos");
        }
        if(ValidateText(tSurname)) 
        {
            $('#feedback').show();
            $('#feedback').text("O campo de sobrenome contém caracteres inválidos");
        }
        else if(ValidateText(tCrm)) 
        {
            $('#feedback').show();
            $('#feedback').text("O campo de CRM contém caracteres inválidos");
        }
        else if(ValidateText(tUf)) 
        {
            $('#feedback').show();
            $('#feedback').text("O campo de UF contém caracteres inválidos");
        }
        else
        {
            if(ValidateEmail(tEmail))
            {
                if(!ValidateText(tName) && !ValidateText(tSurname) && !ValidateText(tCrm) && !ValidateText(tUf))
                {
                    if($("#autorizo").is(":checked") && $("#termos").is(":checked") && $("#aceito").is(":checked"))
                    {
                        $('#loaderStart').show();
                        PostData(tEmail, tName, tSurname, tCrm, tUf);
                    }
                    else 
                    {
                        $('#feedback').show();
                        $('#feedback').text("Aceite os termos para prosseguir");
                    }
                }
                // console.log(tEmail);
                // console.log(tName);
                // console.log(tCompany);
            }
            else 
            {
                // console.log("email invalido");
                $('#feedback').show();
                $('#feedback').text("Digite um email valido");
            }
        }
    }
}

$("#nome").keydown(CheckField);
$("#sobrenome").keydown(CheckField);
// $("#email").keydown(CheckField);
$("#crm").keydown(CheckField);
$("#uf").keydown(CheckField);
$("#nome").focusout(CheckField);
$("#sobrenome").focusout(CheckField);
// $("#email").focusout(CheckField);
$("#crm").focusout(CheckField);


function CheckField()
{
    let email = $("#email").val();
    let name = $("#nome").val();
    let surname = $("#sobrenome").val();
    let crm = $("#crm").val();
    let uf = $("#uf").val();
    email = email.replace(/^\s+/, '').replace(/\s+$/, '');
    name = name.replace(/^\s+/, '').replace(/\s+$/, '');
    surname = surname.replace(/^\s+/, '').replace(/\s+$/, '');
    crm = crm.replace(/^\s+/, '').replace(/\s+$/, '');
    uf = uf.replace(/^\s+/, '').replace(/\s+$/, '');
    if(email === "" || name === "" || surname === "" || crm === "" || uf === "")
    {
        $('#btnCadastro').prop('disabled', true);
        $('#btnCadastro').removeAttr("onclick");
    }
    else
    {
        $('#btnCadastro').prop('disabled', false);
        $('#btnCadastro').attr("onclick", "GetFieldsData()");
    }
}

function ValidateEmail(m_email) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{1,99})+$/.test(m_email)) {
        ////console.log("retornou certo");
        return (true);
    }
    ////console.log("retornou errado");
    return (false);
}
function ValidateText(_text) {
    if (/[.,\/#!$%@\\|\/\^&\*;:{}=\+_`'"¨~^()¬§¢£¹²³ªº°<>\[(.*?)\]]/.test(_text)) {
        ////console.log("retornou certo");
        return (true);
    }
    ////console.log("retornou errado");
    return (false);
}

$(function () {
    AddListenerEnter("emailArea", "btnLogin");
    CaptureAnalytics("/login", "Página Login");

    $(window).keydown(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });
    // CheckDate("login");
});
    
window.onfocus = function () {
    // CheckDate("login");
}

var login;
$("#btnLogin").click(function () {
    $("#emailFeedback").hide();
    // location.href = 'home.html';
    var email = document.getElementById("emailArea").value;
    login = email;
    if (email !== "") {
        $("#loaderStart").show();
        loader_interval = setTimeout(function () {
            email = document.getElementById("emailArea").value;

            clearInterval(loader_interval);

            setTimeout(function () {
                CheckUserAuth(email);
            }, 200);
        }, 200);

    } else {
        $("#emailFeedback").css("background-color", "#E6332A");
        $("#emailFeedback").show();
        $("#emailFeedback").text("Preencha com seu email para começar");
    }
    // if (canLogin) {
    // } else {
    //     $("#emailFeedback").html("A plataforma será liberada dia x de x");
    //     $("#btnLogin").text("Em breve");
    // }
});


function AddListenerEnter(field, button) {
    input = document.getElementById(field);
    input.addEventListener("keyup", function (event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            document.getElementById(button).click();
        }
    });
}
function AddListenerFocus(field, focus) {
    input = document.getElementById(field);
    input.addEventListener("keyup", function (event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            $("#" +focus).focus();
        }
    });
}

$(function() {
    $("form").submit(function() { return false; });
});

AddListenerFocus("nome", "sobrenome");
AddListenerFocus("sobrenome", "crm");
// AddListenerFocus("email", "crm");
AddListenerFocus("crm", "uf");
AddListenerEnter("uf", "btnCadastro");


function ButtonClick(_case){
	switch (_case) {
		// case "pipeline":
		// 	window.open("pdfs/Pipeline_março_2021_DDW.pdf", "_blank");
        //     CaptureAnalytics("/Cadastro/Pipeline", "Abriu o PDF do Pipeline");
		// break;
		case "termos":
			window.open("pdfs/POLÍTICA DE PRIVACIDADE_MC2.pdf", "_blank");
            CaptureAnalytics("/Cadastro/Termos", "Abriu os Termos do cadastro");
		break;
    }
}