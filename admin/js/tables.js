const m_APIEndPoint = "https://29556qsz4c.execute-api.sa-east-1.amazonaws.com/dev/";

var logged = getCookie("logged");

// var Clusterize = require('clusterize.js');


var HttpClientGet = function () {
    this.get = function (aUrl, aCallback) {
        var anHttpRequest = new XMLHttpRequest();
        anHttpRequest.onreadystatechange = function () {
            // alert("Status: " + anHttpRequest.status);
            if (anHttpRequest.readyState == 4 && anHttpRequest.status == 200)
                aCallback(anHttpRequest.responseText);
        }

        anHttpRequest.open("GET", aUrl, true);
        anHttpRequest.send(null);
    }
}

var HttpClientPost = function () {
    this.post = function (aUrl, aCallback) {
        var anHttpRequest = new XMLHttpRequest();
        anHttpRequest.onreadystatechange = function () {
            // alert("Status: " + anHttpRequest.status);
            if (anHttpRequest.readyState == 4 && anHttpRequest.status == 200)
                aCallback(anHttpRequest.responseText);
        }

        anHttpRequest.open("POST", aUrl, true);
        anHttpRequest.send(null);
    }
}

var createContent;

function Login()
{
    GetDatabaseLogin($("#userField").val(), $("#passField").val())
}

function AddListenerFocus(field, focus) {
    input = document.getElementById(field);
    input.addEventListener("keyup", function (event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            $("#" +focus).focus();
        }
    });
}
function AddListenerEnter(field, button) {
    input = document.getElementById(field);
    input.addEventListener("keyup", function (event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            document.getElementById(button).click();
        }
    });
}


var table = "actions"

function CheckCookie()
{
    AddListenerFocus("userField", "passField");
    AddListenerEnter("passField", "buttonLogin");
    $("#scriptId").remove();
    if(getCookie("logged") != "")
    {
        $("#scriptLogin").remove();
        createContent = () =>{
            $("#login").remove();
            $("body").append('<section class="data container rownopad justify-content-start align-items-start" id="data"> <div class=" margin-bot col-12"> <h4 class="title-color">MC2 / Janssen Leading</h4> <h4 class="title-color" id="usersTotal"></h4> <h4 class="title-color" id="users1"></h4> <h4 class="title-color" id="users2"></h4> <h4 class="title-color" id="users3"></h4> <img class="primo" src="imgs/logoPrimo-40px.png"> </div> <header class=""> <nav> <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist"> <a class="nav-item nav-link active" id="tabela01" data-toggle="tab" href="#navTabela01" role="tab" aria-controls="navTabela01" aria-selected="true">Ações</a> <a class="nav-item nav-link" id="tabela02" data-toggle="tab" href="#navTabela02" role="tab" aria-controls="navTabela02" aria-selected="true">Usuários</a> </nav> </header> <div class="col-12"> <div class="tab-content px-sm-0" id="nav-tabContent"> <div class="tab-pane fade show active" id="navTabela01" role="tabpanel" aria-labelledby="tabela01"> <table class="table table-borderless" id="table_actions"> <thead> <tr> <th scope="col">Email</th> <th scope="col">Ação</th> <th scope="col">Hora</th> </tr> </thead> <tbody id="actionsBody"> </tbody> </table> </div> <div class="tab-pane fade show" id="navTabela02" role="tabpanel" aria-labelledby="tabela02"> <table class="table table-borderless" id="table_users"> <thead> <tr> <th scope="col">Email</th> <th scope="col">Nome</th> <th scope="col">CRM</th> <th scope="col">UF</th> <th scope="col">Entrou</th> <th scope="col">Hora</th> </tr> </thead> <tbody id="userBody"> </tbody> </table> </div> </div> </div> <div class="btns"> <button type="button" class="btn" id="btnAtt"><img src="imgs/reloadBtn.png"></button> <button type="button" class="btn" id="btnDownload" onclick="DownloadTable()"><img src="imgs/downloadBtn.png"></button> </div> </section>');
            window.onload = window.onresize = function () { 
                var data = document.getElementById('data'); 
                var height = window.innerHeight; 
                data.style.minHeight = height + 'px'; 
            }; 
            $("#tabela01").attr("onclick", "ChangeTable('actions')");
            $("#tabela02").attr("onclick", "ChangeTable('users')");
            $("#btnAtt").attr("onclick", "GetDatabaseData('"+table+"')");
            GetDatabaseData(table)
            createContent = null;
        }
        setTimeout(() => {
            createContent();
        }, 1000);
    }
    else{
        $("#login").show();
    }
}

function ChangeTable(_table)
{
    if(table == _table) return;
    table = _table;
    $("#btnAtt").attr("onclick", "GetDatabaseData('"+table+"')");
    GetDatabaseData(table);
}

function GetDatabaseLogin(_User, _Password) {
    let url = m_APIEndPoint + "databaselogin" + "?u=" + _User + "&p=" + _Password;
    var client = new HttpClientGet();
    client.get(url, function (response) {
        let temp = JSON.parse(response);
        console.log(temp)

        if(temp == 1)
        {
            $("#scriptLogin").remove();
            createContent = () =>{
                $("#login").remove();
                document.cookie = "logged=true; expires=Fri, 31 Dec 9999 23:59:59 GMT";
                $("body").append('<section class="data container rownopad justify-content-start align-items-start" id="data"> <div class=" margin-bot col-12"> <h4 class="title-color">MC2 / Janssen Leading</h4> <h4 class="title-color" id="usersTotal"></h4> <h4 class="title-color" id="users1"></h4> <h4 class="title-color" id="users2"></h4> <h4 class="title-color" id="users3"></h4> <img class="primo" src="imgs/logoPrimo-40px.png"> </div> <header class=""> <nav> <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist"> <a class="nav-item nav-link active" id="tabela01" data-toggle="tab" href="#navTabela01" role="tab" aria-controls="navTabela01" aria-selected="true">Ações</a> <a class="nav-item nav-link" id="tabela02" data-toggle="tab" href="#navTabela02" role="tab" aria-controls="navTabela02" aria-selected="true">Usuários</a> </nav> </header> <div class="col-12"> <div class="tab-content px-sm-0" id="nav-tabContent"> <div class="tab-pane fade show active" id="navTabela01" role="tabpanel" aria-labelledby="tabela01"> <table class="table table-borderless" id="table_actions"> <thead> <tr> <th scope="col">Email</th> <th scope="col">Ação</th> <th scope="col">Hora</th> </tr> </thead> <tbody id="actionsBody"> </tbody> </table> </div> <div class="tab-pane fade show" id="navTabela02" role="tabpanel" aria-labelledby="tabela02"> <table class="table table-borderless" id="table_users"> <thead> <tr> <th scope="col">Email</th> <th scope="col">Nome</th> <th scope="col">CRM</th> <th scope="col">UF</th> <th scope="col">Entrou</th> <th scope="col">Hora</th> </tr> </thead> <tbody id="userBody"> </tbody> </table> </div> </div> </div> <div class="btns"> <button type="button" class="btn" id="btnAtt"><img src="imgs/reloadBtn.png"></button> <button type="button" class="btn" id="btnDownload" onclick="DownloadTable()"><img src="imgs/downloadBtn.png"></button> </div> </section>');
                window.onload = window.onresize = function () { 
                    var data = document.getElementById('data'); 
                    var height = window.innerHeight; 
                    data.style.minHeight = height + 'px'; 
                }; 
                $("#tabela01").attr("onclick", "ChangeTable('actions')");
                $("#tabela02").attr("onclick", "ChangeTable('users')");
                $("#btnAtt").attr("onclick", "GetDatabaseData('"+table+"')");
                GetDatabaseData(table)
                createContent = null;
            }
            createContent();
        }
        else
        {
            $("#errorFeedback").text("* Usuário ou senha incorretos")
        }
    });
}

var group1 = 0;
var group2 = 0;
var group3 = 0;

function GetDatabaseData(_table) {
    let url = m_APIEndPoint + "databasedata" + "?t=" + _table;
    var client = new HttpClientGet();
    let email = [];
    let name = [];
    let crm = [];
    let uf = [];
    let action = [];
    let time = [];
    group1 = 0;
    group2 = 0;
    group3 = 0;
    $("#loaderStart").show();
    client.get(url, function (response) {
        let temp = JSON.parse(response);
        if(_table == "actions")
        {
            for(var i = 0; i < temp.length; i++){
                email.push(temp[i].email);
                action.push(temp[i].action);
                var tempTime = temp[i].timestamp;
                tempTime = tempTime.replace("T", " ");
                tempTime = tempTime.replace(".000Z", "");
                tempTime = tempTime.split(" ");
                let date = tempTime[0];
                let hour = tempTime[1];
                date = date.split("-");
                hour = hour.split(":");
                if(hour[0] == 01) {
                    hour[0] = 22;
                    date[2] = date[2] - 1;
                }
                else if(hour[0] == 02) {
                    hour[0] = 23;
                    date[2] = date[2] - 1;
                }
                else {
                    hour[0] = hour[0] - 3;
                    if(hour[0] == -3) {
                        hour[0] = 0;
                    }
                    else if(hour[0] == -2) {
                        hour[0] = 1;
                    }
                    else if(hour[0] == -1) {
                        hour[0] = 2;
                    }
                }
                date = date[2] + "/" + date[1] + "/" + date[0]
                hour = hour[0] + ":" + hour[1] + ":" + hour[2]
                let finalTime = hour + " - " + date;
                time.push(finalTime);
            }
            $("#table_"+_table+" > tbody").empty();
            CreateTable(email, action, time, _table)
        } 
        else if(_table == "users"){
            for(var i = 0; i < temp.length; i++){
                email.push(temp[i].email);
                if(temp[i].accountcreated == 1){
                    name.push(temp[i].name);
                    crm.push(temp[i].crm);
                    uf.push(temp[i].uf);
                }
                else{
                    name.push(" - ");
                    crm.push(" - ");
                    uf.push(" - ");
                }
                var tempTime = temp[i].time;
                tempTime = tempTime.replace("T", " ");
                tempTime = tempTime.replace(".000Z", "");
                tempTime = tempTime.split(" ");
                let date = tempTime[0];
                let hour = tempTime[1];
                date = date.split("-");
                hour = hour.split(":");
                if(hour[0] == 01) {
                    hour[0] = 22;
                    date[2] = date[2] - 1;
                }
                else if(hour[0] == 02) {
                    hour[0] = 23;
                    date[2] = date[2] - 1;
                }
                else {
                    hour[0] = hour[0] - 3;
                    if(hour[0] == -3) {
                        hour[0] = 0;
                    }
                    else if(hour[0] == -2) {
                        hour[0] = 1;
                    }
                    else if(hour[0] == -1) {
                        hour[0] = 2;
                    }
                }
                date = date[2] + "/" + date[1] + "/" + date[0]
                hour = hour[0] + ":" + hour[1] + ":" + hour[2]
                let finalTime = hour + " - " + date;
                time.push(finalTime);
                if(temp[i].haslogged == 0) {
                    action.push("Não");
                }
                else if(temp[i].haslogged == 1) {
                    action.push("Sim");
                }
                if(temp[i].room == 1) group1 ++;
                if(temp[i].room == 2) group2 ++;
                if(temp[i].room == 3) group3 ++;
            }
            $("#table_"+_table+" > tbody").empty();
            CreateTableUser(email, name, crm, uf, action, time, _table)
        }
    });
}

function CreateTable(_email, _action, _time, _table) {
    for (var i = 0; i < _email.length; i++) {
        $("#table_"+_table).find('tbody').append("<tr><td>" + _email[i] + "</td><td>" + _action[i] + "</td><td>" + _time[i] + "</td></tr>");
    }
    $("#usersTotal").text("");
    $("#users1").text("");
    $("#users2").text("");
    $("#users3").text("");
    $("#loaderStart").hide();
}

function CreateTableUser(_email, _name, _crm, _uf, _action, _time, _table) {
    for (var i = 0; i < _email.length; i++) {
        $("#table_"+_table).find('tbody').append("<tr><td>" + _email[i] + "</td><td>" + _name[i] + "</td><td>" + _crm[i] + "</td><td>" + _uf[i] + "</td><td>" + _action[i] + "</td><td>" + _time[i] + "</td></tr>");
    }
    $("#usersTotal").text("Total de Usuário: " + _email.length);
    $("#users1").text("Grupo 1: " + group1);
    $("#users2").text("Grupo 2: " + group2);
    $("#users3").text("Grupo 3: " + group3);
    $("#loaderStart").hide();
}

function DownloadTable(){
    $('#table_'+table).table2csv();
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }